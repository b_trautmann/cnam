#include <stdio.h>

int puissXIte (int x, int n) {

	int i,r=1;
	for(i=0;i<n;i++){
		r= r * x;
	}
	return r;
}

int puissXRec1 (int x, int n) {
	if(n==0){return 1;}
	else {return (x*puissXRec1(x,n-1));}
}

int puissXRec2 (int x, int n){	
	if(n==0){return 1;}
	else{ 
		if(n%2==0){
			return (puissXRec2(x,n/2)*puissXRec2(x,n/2));
		} else {
			return x*puissXRec2(x,n-1);
		}
	}
}

int puissXRec3 (int x, int n) {

	if(n==0){return 1;}
	else{
		if(n%2==0){
			return (puissXRec3(x*x,n/2));
		} else {
			return x*puissXRec3(x,n-1);
		}
	}
}

void main() {
	int x = 2;

	int n = 12;

	printf("x = %d, n = %d\n",x,n);

	printf("Res1 = %d\n",puissXIte(x,n));

	printf("Res2 = %d\n",puissXRec1(x,n));

	printf("Res3 = %d\n",puissXRec2(x,n));

	printf("Res4 = %d\n",puissXRec3(x,n));

	n = 1;
	while(puissXRec3(10,n)<puissXRec3(10,n+1)){
		n++;	
	}
	printf("n = %d\n",n);

	return;
}
