#include <stdio.h>	
#include <time.h>
/*#include <stdlib.h>
#include <windows.h>*/
#define B 0
#define W 1
#define R 2
#define tailleT 125
#define KRED  "\x1B[31m"
#define KBLU  "\x1B[34m"
#define KWHT  "\x1B[37m"


/*void Color(int couleurDuTexte,int couleurDeFond) // fonction d'affichage de couleurs
{
        HANDLE H=GetStdHandle(STD_OUTPUT_HANDLE);
        SetConsoleTextAttribute(H,couleurDeFond*16+couleurDuTexte);
}*/

void afficherTab(int t[], int n) {
	
	int i;

	printf("{");
	for(i=0; i < n; i++) {
		printf(" %d", t[i]);
	}
	printf(" }\n");
}

void plusGrand(int t[], int n) {

	int i;
	int pG = t[0];

	for(i=1; i<n ;i++) {
		if(t[i] > pG) {
			pG = t[i];
		}
	}

	printf("Plus grand = %d \n", pG);
}

void deuxiemePlusGrand(int t[], int n)
{
	int i;
	int res[2] = {t[0],t[1]};

	for(i = 1; i < n; i++) {
		if(t[i] > res[0]) {
			res[1] = res[0];
			res[0] = t[i];
		} else if(t[i] > res[1]) {
			res[1] = t[i];
		}
	}

	printf("Deuxième plus grand = %d \n", res[1]);
}

void moyenne (int t[], int n) {

	int i;
	float total = 0;

	for(i=0;i<n;i++){
		total += t[i];
	}

	printf("La moyenne est : %f \n", total/n);
}

int appartenance(int t[], int n, int val) {


	int i;

	for(i=0; i< n; i++) {

		if(t[i] == val) {
			printf("La valeur %d appartient au tableau \n", val);
			return 1;
		}
	}

	printf("La valeur %d n'appartient pas au tableau \n", val);
	return 0;

}

void afficherDrapeau(int t[], int n, int j) {

	int i = 0;

	printf(" [");
	for(i; i< n; i++){
		if( t[i] == B)
		{
			if(i == j){
				printf("%s#", "\x1B[32m");
			} else {
				printf("%s#", KBLU);
			}

		}
		else if ( t[i] == W) {
			if(i == j){
				printf("%s#", "\x1B[32m");
			} else {
				printf("%s#", KWHT);
			}
		} else {

			if(i == j){
				printf("%s#", "\x1B[32m");
			} else {
				printf("%s#", KRED);
			}
		}
	}
	printf("%s]\n", "\x1B[30m");
}

void echange(int t[], int n, int i, int j){
	int e = t[i];
	t[i] = t[j];
	t[j] = e;
}

void trierDrapeau(int t[], int n) {

	int iB =0; int iR = n-1;
	int i = 0;

	while(i<=iR){
		afficherDrapeau(t,n, i);
		if(t[i] == B){
			echange(t,n,i,iB);
			iB++;i++;
		} else if (t[i] == R){
			echange(t,n,i,iR);
			iR--;

		} else {i++;}

	}
	printf("Tableau final ! :");
	afficherDrapeau(t,n, 0);

}

void triEchange(int t[], int n) {

	int i; int valE; int j;

	printf("TRI ECHANGE :\n");
	afficherTab(t,n);
	for(i=n; i > 0; i--) {
		for(j=1; j < i; j++){
			if(t[j-1] > t[j]) {
				echange(t,n,j,j-1);
			}
		}
	}
	afficherTab(t,n);
}

void triSelect(int t[], int n) {
	int i,j,e;	

	printf("TRI SELECT :\n");
	afficherTab(t,n);

	for(i=0;i<n;i++){
		e = i;
		for(j=i; j<n; j++){
			if(t[j]<t[e]){
				e = j;
			}
		}
		echange(t,n,i,e);

	}

	afficherTab(t,n);

}

void main() {

	srand(time(NULL));

	int i;
	int tab[tailleT], tab2[tailleT], drapeau[tailleT];

	for( i=0;i<tailleT;i++){
		tab[i] = rand()%100;
		tab2[i] = rand()%100;
		drapeau[i] = rand()%3;
	}

	afficherTab(tab, tailleT);
	
	plusGrand(tab,tailleT);

	deuxiemePlusGrand(tab, tailleT);

	moyenne(tab, tailleT);

	for(i=0; i<10; i++){
		appartenance(tab, tailleT, rand()%100);
	}

	afficherDrapeau(drapeau, tailleT, 0);

	trierDrapeau(drapeau, tailleT);

	triEchange(tab, tailleT);

	triSelect(tab2,tailleT);
	
}
