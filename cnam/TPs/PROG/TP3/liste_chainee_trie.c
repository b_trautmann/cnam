#include <stdio.h>
#include <stdlib.h>
#include "liste_chainee.h"

void afficher_liste(sliste *l){

	if(l!=NULL){
		if(l->s == NULL){
			printf("[ %d ]",l->e);
			printf("\n");
		} else {
			printf("[ %d ]=>",l->e);
			afficher_liste(l->s);
		}	
	}
	
}

int tete_liste(sliste *l){
	
	return(l->e);
}

sliste * liste_vide(){
	
	return NULL;
}

int recherche(sliste *l, int c) {

	if(l==NULL){
		return 0;
	} else {
		return ((l->e == c) || recherche(l->s,c));
	}
}

sliste * ajout_tete(int e, sliste *l){

	sliste * new_l = (sliste *)malloc(sizeof(sliste));

	new_l->s = l;
	new_l->e = e;
	return new_l;	
}

sliste * suppression(sliste *l, int c){
	if(l==NULL){
		return NULL;
	} else if((l->s)->e < c) {
		return ajout_tete(l->e, suppression(l->s,c));
	} else if((l->s)->e == c){
		
		sliste * del = l->s;
		l->s = (l->s)->s;
		free(del);

		return l;
	}
}

sliste * ajout(sliste * l, int c){

	if(l==NULL){
		
		sliste * m = (sliste *)malloc(sizeof(sliste));
		m->e = c;
		m->s = NULL;
		return m;
	} else if(l->e < c) {
		return ajout_tete(l->e, ajout(l->s,c));
	} else {
		sliste *m = (sliste *)malloc(sizeof(sliste));
		m->s = l->s;
		l->s = m;
		m->e = l->e;
		l->e = c;
		return l;
	}
}

sliste * position_insertion(sliste *l, int c){

	if(l!=NULL){

		if((l->s == NULL) || (((l->s)->e) > c)){
			return l;
		} else {
			return position_insertion(l->s,c);
		}
	} else {
		return NULL;
	}
}

sliste * insertion(sliste *l, int c){

	if(l==NULL){
		return ajout_tete(c,NULL);
	} else if (l!=NULL) {
		sliste *p = position_insertion(l,c);
		if(p==l){
			return ajout_tete(c,l);
		} else {
			sliste * new_l = (sliste *)malloc(sizeof(sliste));
			new_l->s = p->s;
			new_l->e = c;
			p->s = new_l;
			return l;
		}
	}
}
