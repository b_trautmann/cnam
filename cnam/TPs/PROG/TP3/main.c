#include <stdio.h>
#include <stdlib.h>  // pour srand et rand
#include <time.h>    // pour srand

#include "liste_chainee.h"

void main(){

	srand(100);

	printf("-----------------AJOUT-----------------\n\n");
	sliste *l = liste_vide();

	l = ajout(l,2);
	l = ajout(l,3);
	l = ajout(l,4);
	afficher_liste(l);
	l = ajout(l,1);
	afficher_liste(l);
	
	printf("\n");

	sliste *l2 = liste_vide();

	l2 = ajout(l2,0);
	l2 = ajout(l2,1);
	l2 = ajout(l2,3);
	l2 = ajout(l2,4);
	afficher_liste(l2);
	l = ajout(l2,2);
	afficher_liste(l2);

	printf("\n");

	sliste *l3 = liste_vide();

	l3 = ajout(l3,2);
	l3 = ajout(l3,3);
	afficher_liste(l3);
	l3 = ajout(l3,7);
	afficher_liste(l3);

	printf("\n----------------RECHERCHE---------------\n\n");

	sliste *l4 = liste_vide();
	
	int i = 0;
	while(i<15){
		l4 = ajout(l4,rand()%20);
		i++;
	}
	afficher_liste(l4);
	int intR1 = rand()%20;
	int intR2 = rand()%20;
	int intR3 = rand()%20;
	
	printf("\nRecherche de %d : %d \n\n", intR1, recherche(l4,intR1));
	printf("Recherche de %d : %d \n\n", intR2, recherche(l4,intR2));
	printf("Recherche de %d : %d \n\n", intR3, recherche(l4,intR3));

	printf("----------------SUPPRESSION-------------\n\n");

	l4 = suppression(l4,11);
	afficher_liste(l4);
	printf("\n");
	l4 = suppression(l4,19);
	afficher_liste(l4);
	printf("\n");
	l4 = suppression(l4,0);
	afficher_liste(l4);
	printf("\n");
	
}
