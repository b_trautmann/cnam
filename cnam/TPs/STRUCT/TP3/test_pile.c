#include <stdio.h>
#include <stdlib.h>  // pour srand et rand
#include <time.h>    // pour srand
#include "pile.h"
#include "strings.h"

void main(){
	
	char input[100] = "";
	
	pile p = pileNouv();
	
	scanf("%s",&input);
	while(estVide(p)!=1 || strcasecmp(input,"up")!=0) {
		
		if(strcasecmp(input,"up") == 0){
			printf("Dépilé : %s\n",sommet(p));
			depiler(p);
			printf("Taille de la pile %d\n",taille(p));
		} else {
			empiler(p,input);
			printf("Taille de la pile %d\n",taille(p));
			printf("Empilé : %s\n",sommet(p));
		}

		scanf("%s",&input);
	}
	printf("Pile vide\n");
}
