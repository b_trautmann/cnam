#include <stdio.h>
#include <stdlib.h>
#include "arbre.h"

arbre nouvArbre(){

	/*arbre a = (arbre *)malloc(sizeof(arbre));
	a->val = NULL;
	a->fg = NULL;
	a->fd = NULL;
	return a;*/
	return NULL;
}

arbre enrac(T racine, arbre fg, arbre fd){

	arbre a = (arbre *)malloc(sizeof(struct NOEUD));
	a->val = racine;
	a->fg = fg;
	a->fd = fd;
	return a;
}

arbre creerFeuille(T val) {

	arbre a = (arbre *)malloc(sizeof(struct NOEUD));
	a->val = val;
}

T racine(arbre a){
	return a->val;
}

arbre fg(arbre a){
	return a->fg;
}

arbre fd(arbre a){
	return a->fd;
}

int taille(arbre a){
	int taille_fg;
	int taille_fd;

	if(a == NULL) {
		return 0;
	} else if(a->fg == NULL && a->fd == NULL){
		return 1;
	} else {
		taille_fg = taille(a->fg);
		taille_fd = taille(a->fd);

		return taille_fg+taille_fd;
	}
}

int estVide(arbre a){

	if(a==NULL){
		return 1;
	} else {
		return 0;
	}
}

int estFeuille(arbre a){
	if(a->val != NULL && a->fg == NULL && a->fd == NULL) {
		return 1;
	} else {
		return 0;
	}
}

int hauteur(arbre a){

	int hauteur_fg;
	int hauteur_fd;
	if(a==NULL) {
		return 0;
	} else if (a->fg == NULL && a->fd == NULL) {
		return 1;
	} else{
		hauteur_fg = hauteur(a->fg);
		hauteur_fd = hauteur(a->fd);

		return hauteur_fg > hauteur_fd ? hauteur_fg : hauteur_fd;
	}
}

void parcoursPrefixe(arbre a, void (*affiche)(T)){


	if(!estVide(a)){
		affiche(a->val);
		parcoursPrefixe(a->fg,affiche);
		parcoursPrefixe(a->fd,affiche);
	}
	return;
}

void parcoursInfixe(arbre a, void (*affiche)(T)){
	if(!estVide(a)){
		parcoursInfixe(a->fg,affiche);
		affiche(a->val);
		parcoursInfixe(a->fd,affiche);
	}
	return;
}

void parcoursPostfixe(arbre a, void (*affiche)(T)){

	if(!estVide(a)){
		parcoursPostfixe(a->fg,affiche);
		parcoursPostfixe(a->fd,affiche);
		affiche(a->val);
	}
	return;
}

void parcoursGenerationDot(arbre a){

	FILE * fp;

	fp = fopen("arbre.dot","w+");

	lancementParcoursPrefixeDot(a,fp);
	fclose(fp);
}

void lancementParcoursPrefixeDot(arbre a, FILE * fp){
	if(!estVide(a)){

		fprintf(fp,"digraph arbre{\n%d[label=\"%d\"];\n",a,a->val);
		parcoursPrefixeDot(a->fg,a,fp);
		parcoursPrefixeDot(a->fd,a,fp);
		fprintf(fp,"}");
	}
	return;
}

void parcoursPrefixeDot(arbre a, arbre pointeurParent, FILE * fp){

	int iNouvParent;
	if(!estVide(a)){
		printf("%d -> %d;",pointeurParent,a);
		fprintf(fp,"%d[label=\"%d\"];\n",a,a->val);
		fprintf(fp,"%d -> %d;\n",pointeurParent,a);
		parcoursPrefixeDot(a->fg,a,fp);
		parcoursPrefixeDot(a->fd,a,fp);
		
	}
	return;
}

