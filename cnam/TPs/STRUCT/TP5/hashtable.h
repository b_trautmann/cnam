#ifndef HASHTABLE_H_INCLUDED
#define HASHTABLE_H_INCLUDED

#define HASH_SIZE   10

typedef int key_t;
typedef char value_t;
typedef int bool;

enum { false, true };

typedef struct {
    key_t k;
    value_t v;
} entry;

typedef struct HASHTABLE {
    entry entries[HASH_SIZE];
    int next[HASH_SIZE];
    bool free[HASH_SIZE];
} *hashtable;

hashtable newHashTable();
void insert(hashtable, key_t, value_t);
void delete(hashtable, key_t);
bool get(hashtable, key_t, value_t *);
void modify(hashtable, key_t, value_t);
void display(hashtable);

#endif // HASHTABLE_H_INCLUDED
