#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

hashtable newHashTable(){
	hashtable h;
	h = (hashtable *)malloc(sizeof(struct HASHTABLE));

	int i;	
	for(i=0;i<HASH_SIZE;i++){
		h->free[i]==0;
		h->next[i]==-1;
	}

	return h;
}

static int hash(key_t k){
	return k%HASH_SIZE;
}

static void record(hashtable ht, key_t k, value_t v, int index){
	ht->entries[index].v = v;
	ht->entries[index].k = k;
	ht->next[index] = -1;
	if(v==NULL){
		ht->free[index] = 0;
	} else {
		ht->free[index] = -1;
	}
}

static int get_first_free(hashtable ht){

	int i;	
	for(i=0;i<HASH_SIZE;i++){

		if(ht->free[i]==0){
			return i;
		}
	}
	return -1;
}

static int get_previous(hashtable ht, key_t k, int index_sought){

	int search = hash(k);
	int res = search;

	while(search!=index_sought){
		res = search;
		search = ht->next[res];
	}

	return res;
}



void insert(hashtable h, key_t k, value_t v){

	int index = hash(k);
	int parcours;
	int next;
	if(h->free[index] == 0){
		record(h,k,v,index);
	} else {
		next = get_first_free(h);

		if(next == -1){
			return 0;
		}

		if(h->next[index] == -1){
			h->next[index] = next;
		} else {
			parcours = h->next[index];
			while(parcours != -1){
				parcours = h->next[parcours];
			}
			h->next[parcours] = next;
		}
		record(h, k, v, next);
	}
}

void delete(hashtable h, key_t k) {
	
	int index = hash(k);
	
	if(h->entries[index].k == k){
		record(h,NULL,NULL,index);
	} else {
		while(h->entries[index].k != k){
			index = h->next[index];
		}
		int previous = get_previous(h, k, index);
		h->next[previous] = h->next[index];
		record(h,NULL,NULL,index);
	}
	
}

bool get(hashtable h, key_t k, value_t * v){
	
	int index = hash(k);
	bool res = 0;

	while(h->entries[index].k != k && h->entries[index].k != NULL){
		index = h->next[index];
	}

	if( h->entries[index].k == k){
		*v = h->entries[index].v;
		return 0;
	} else {
		return -1;
	}
}

void modify(hashtable h, key_t k, value_t v){

	int index = hash(k);
	bool res = 0;

	while(h->entries[index].k != k && h->entries[index].k != NULL){
		index = h->next[index];
	}

	if( h->entries[index].k == k){
		h->entries[index].v = v;
	}

}

void display(hashtable h){
	int i;
	for(i=0;i<HASH_SIZE;i++){
		printf("%d ==> [%d | %d] [%d] [%d]\n",i,h->entries[i].k,h->entries[i].v,h->next[i],h->free[i]);
	}
}
