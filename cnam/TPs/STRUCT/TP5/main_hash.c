#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"


void main(){

	int * res;

	hashtable h = newHashTable();

	insert(h,14,8);
	insert(h,54,45);
	insert(h,6,78);
	insert(h,9,12);
	insert(h,11,4);
	insert(h,0,6);
	insert(h,1,5);

	delete(h,9);
	delete(h,1);

	display(h);

	modify(h, 11, 89);
	printf("\n\n");

	display(h);

	// A DEBUG
	if(get(h,54,res) == 0){
		printf("TROUVE");
	}
	
}
