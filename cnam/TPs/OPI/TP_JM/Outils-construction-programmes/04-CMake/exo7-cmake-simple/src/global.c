/* Programme permettant d'effectuer des operations sur des 
polynomes de coefficients entiers. Chaque polynome est memorise
a l'aide d'un tableau. L'element i du tableau comporte le
coefficient de x a la puissance i. Pour eviter des allocations
dynamiques de memoire, ces tableaux sont de taille constante N.
Le degre du polynome verifie la condition degre<N. Ce degre peut
etre demande a l'utilisateur lors de la saisie ou etre calcule
(pour tout i, degre<i<N, p[i]=0). Pour ne pas dissocier degre
et coefficient d'un meme polynome, une structure de donnees 
est definie */

#include <stdio.h>
#include "defs.h"

float valeur (struct polynome, float);
void produit(void);

/* ************************************************
   saisie
   saisie du degre et des coefficients d'un polynome
   entree : -
   sortie : polynome 
**************************************************** */
struct polynome saisie (void) {
  struct polynome P;
  int i;
  printf("Veuillez donner le degre du polynome : ");
  scanf("%d",&P.degre);
  for(i=1+P.degre; i--; ) {
     printf("donner le coefficient de x a la puissance %d : ",i);
     scanf("%d", &P.coef[i]);
  }
  printf("\n");
  return P;
}


/* ************************************************
   evaluation
   Saisie des coefficient d'un polynome P et calcul
   de P(X) pour un ensemble de X donnes
   entree : -
   sortie : -
**************************************************** */
void evaluation(void) {
  struct polynome P;
  int encore;
  float X;

  P=saisie();
  do {
     printf("Donner la valeur de X : ");
     scanf ("%f", &X);
     printf("P(%f)=%f\n",X,valeur(P,X));
     printf("Calcul pour une autre valeur de X (O/N) ? ");
     getchar();
     encore=getchar();
  } while (encore=='o' || encore=='O');
}

/* ************************************************
   texte
   menu principal affiche  a l'utilisateur
   entree : -
   sortie : choix (pas de verification de validite)
**************************************************** */
int texte (void) {
  int choix;  
  printf("\n");
  printf("1.	Evaluation P(X)\n");
  printf("2.	Produit de 2 polynome\n");
  printf("3.	Arret\n");
  printf("Votre choix : ");
  scanf("%d",&choix);
  printf("\n");
  return choix;
}

/* ************************************************
   menu
   menu principal affiche a l'utilisateur. Fait appel
   a texte(). 
   entree : -
   sortie : choix (verification de sa validite)
**************************************************** */
int menu (void) {
  int choix;
  choix=texte();
  while (choix!=1 && choix!=2 && choix!=3) {
    printf("\aMauvais choix, il ne peut etre realise...\n");
    choix=texte();
  }
  return choix;
}

/* ************************************************
   main 
   Affichage menu principal et appel suivant le 
   cas a evaluation de P(X) ou au produit de 2
   polynome. Boucle tant que l'utilisateur ne 
   demande pas l'arret.
   entree : -
   sortie : -
**************************************************** */  
int main (void) {
  int choix;
  choix=menu();
  do {
    switch (choix) {
       case 1 : evaluation(); 
                break;
       case 2: produit();
               break;
       case 3: return(1);
    }
    choix=menu();
   } while (1);
}

