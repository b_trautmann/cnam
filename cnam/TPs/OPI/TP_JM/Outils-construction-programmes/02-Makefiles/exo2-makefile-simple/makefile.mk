vpath %.c ./src ./lib
vpath %.h ./header

CC = gcc
CFLAGS = -Wall
LDFLAGS = -Wall -lm
EXEC = global

all: $(EXEC)

global: global.o libtest.a
	$(CC) -o global $< $(LDFLAGS) libtest.a

libtest.a: valeur.o mult.o produit.o
	ar -rv libtest.a $^

%.o: %.c defs.h
	$(CC) -o $@ -c $< $(CFLAGS) -I./header
