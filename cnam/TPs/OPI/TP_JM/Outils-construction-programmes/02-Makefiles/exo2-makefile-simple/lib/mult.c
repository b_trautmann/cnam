#include "defs.h"


/* ************************************************
   mult
   multiplication de 2 polynomes
   entree : P1, P2
   sortie : Q=P1*P2
**************************************************** */
struct polynome mult (struct polynome P1, struct polynome P2) {
   struct polynome Q;
   int i1,i2,i;
   Q.degre=P1.degre+P2.degre;
   for (i=0; i<=Q.degre; i++) {
      Q.coef[i]=0;
   }
   for (i1=0; i1<=P1.degre; i1++) {
      for (i2=0; i2<=P2.degre; i2++) {
  	  Q.coef[i1+i2]+=P1.coef[i1]*P2.coef[i2];
      }
    }
    return Q;
}

