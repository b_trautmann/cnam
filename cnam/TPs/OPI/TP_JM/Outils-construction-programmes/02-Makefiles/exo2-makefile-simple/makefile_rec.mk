export CC=gcc
export CFLAGS=-Wall 
export LDFLAGS=-Wall 
SRC_DIR=src 

EXEC=$(SRC_DIR)l

all: $(EXEC) 

$(EXEC): 

cd $(SRC_DIR) && $(MAKE) $@

.PHONY: clean mrproper $(EXEC) 

clean: 

cd $(SRC_DIR) && $(MAKE) $@

mrproper: clean 

cd $(SRC_DIR) && $(MAKE) $@
