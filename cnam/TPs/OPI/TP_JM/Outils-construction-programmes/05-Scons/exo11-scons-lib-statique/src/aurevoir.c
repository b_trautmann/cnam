#include <stdio.h>
#include "parler.h"
#include "speak.h"

int main (void) {

#ifdef FRANCAIS
	dire_aurevoir();
#endif

#ifdef ANGLAIS
	say_goodbye();
#endif

return 1;

}
