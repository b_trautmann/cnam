; { 1 } Alignement.
;	Soit un cache dont les lignes font 128 octets.
;	Donner l'adresse du premier mot de la ligne contenant l'adresse
;		0xA23847EF
;		0x7245E824
;		0xEEFABCD2
;	128 = 2^7 --> les 7 dernier bits donnent l'offset (n°) de la ligne de 2^7 = 128 octets
;
;	0xA23847EF => EF => 1110.1111 => [1]000.0000 } 80
;	0x7245E824 => 24 => 0010.0100 => [0]000.0000 } 00
;	0xEEFABCD2 => D2 => 1101.0010 => [1]000.0000 } 80
;
; { 2 } Lignes et longueur de cache.
;	Soit un cache de 32Ko
;	Combien peut-il contenir de lignes de
;		32 octets
;		64 octets
;		128 octets
;
; { 3 } Si un cache de 16Ko a une longueur de ligne de 128
;	Combien d'ensemble le cache possède t'il pour une associativité de 2/4/8?
;	NB lignes total = 16Ko/128 = 128
;			(2¹⁰*2⁴) / 2⁷	= 2⁷ = 128
;			2 -> 128/2	= 64
;			4 -> 128/4	= 32
;			8 -> 128/8 	= 16	
;
; { 4 } Taille du tableau des etiquettes
;	Soit un cache de 64Ko, avec des lignes de 128 octets et un degré d'associativité
;	de 4. Les addresses sont sur 32 bits.
;
;	-> Combien de lignes dans le cache 64Ko/128 = (2¹⁰x2⁶)/2⁷ = 2⁹ = 512
;	-> Combien d'étiquettes dans les caches : autant que de ligne = 512
;	-> Quelle est la taille mémoire totale occupée par les étiquettes 
;	
;	Taille étiquette = 18 bits 	512x18/8 = 64*18 = 640+512 = 1152 octets
;					(lignes * bits) / octes
; { 5 } Adressage 
;
;	Soit un cache de 64Ko, avec des lignes de 128 octets et un degré d'associativité
;	de 4. Les adresses sont sur 32 bits.
;
;	Donner le n° lignes / offset / voie? pour les adresses
;
;			ligne 		offset
;	0xABC89987  |  	  51	  |	 7	 | 	1001.1001.1000.0111
;	0x32651987  |	  51	  |	 7 	 |      0001.1001.1000.0111
;	0x228945DB  |  	  11	  | 	 91	 | 	0100.0101.1101.1011
;	0x48569CAC  |     57	  | 	 44	 | 	1001.1100.1010.1100
