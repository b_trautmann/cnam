;
;	Calcule si le premier paramètre est un palindrome
;

EXTERN puts
EXTERN printf
EXTERN strlen

SECTION .TEXT
        GLOBAL  main

main:
        mov ebp, esp
        add ebp,4                    ; saute adresse de retour

        mov eax,[ebp]                ; recupere argc
        dec eax	                     ; eax = argc - 2	
        dec eax                      ; si eax diff de 2
        jnz end_err                  ; alors on sort

        add ebp,4                    ; passe a argv dans la pile
        mov esi,[ebp+4]                ; esi <- argv

	push esi		; On passe ESI en paramètre à STRLEN
	call strlen		; Appel à STRLEN
	add esp,4		; Dépile
	
	add edi,esi,eax		; On passe esi + la longueur de la chaine à EDI
	
	boucle:                      	; TQ (ecx != 0)
	
		test edi,esi 	; On teste si la valeurs des deux caractères correspondent
		jnz end_false	; Si ce n'est pas le cas arret en false
		inc esi		; Décrémentation du pointeur esi
		dec edi		; Incrémentation du pointeur edi
		
	loop boucle
	ret
	
end_false:
	ret
	

