;
;	Calcule la longueur d'une chaine
;

EXTERN puts
EXTERN printf

SECTION .TEXT
        GLOBAL  main

main:
        mov ebp, esp
        add ebp,4                    ; saute adresse de retour

        mov eax,[ebp]                ; recupere argc
        dec eax	                     ; eax = argc - 2	
        dec eax                      ; si eax diff de 2
        jnz end                  ; alors on sort

        add ebp,4                    ; passe a argv dans la pile
        mov esi,[ebp]                ; esi <- argv
	mov edi,0

	mov ecx,32              	 ; ecx = 32

boucle:                      	; TQ (ecx != 0)


	mov ebx, [esi]		 ; 	On récupère le caractère courant
	inc esi                  ;      ptr_ch++;
	inc edi			 ; 	On incrémente le compteur de caractères

    	push ebx                 ; 	empile param
    	call printf              ; 	appel printf
    	add esp,4                ; 	depile 

	cmp ecx,0		 ; 	On teste si le caractère courant est égal à 0
	jnz end			 ;	Si oui on termine la boucle

	loop boucle               ;  	Sinon on boucle tant que exc > 0


end:
        add esp,4
	ret
