EXTERN printf
EXTERN puts
EXTERN atoi
SECTION .DATA

chainint db "%d ",13,10,0

SECTION .TEXT
        GLOBAL  main

main:
        mov ebp, esp
        add ebp,4                    ; saute adresse de retour

        mov eax,[ebp]                ; recupere argc
        dec eax	                     ; eax = argc - 2	
        dec eax                      ; si eax diff de 2
        jnz end_err                  ; alors on sort

        add ebp,4                    ; passe a argv dans la pile
        mov esi,[ebp]                ; esi <- argv
        mov eax,[esi+4]              ; eax <- argv[1] 1=>+4 (car int = 4 octets)

	mov edx,32              	 ; ecx = 32

	mov ebx,0

boucle:                  	 ; TQ (edx != 0)

	imul ebx,ebx,2		; On multiplie par 2 la valeur

	test ebx,0		
	jnz else
	add ebx,1
	jmp endif
endif:
	cmp ecx,0		 ; 	On teste si le caractère courant est égal à 0
	jnz end			 ;	Si oui on termine la boucle
	loop boucle


end:
	ret
