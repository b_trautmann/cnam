EXTERN	printf

SECTION .data
	a	dd  5
	b	dd  9
	chainint db "%d",13,10,0
	chaine	db  "Hello world !",13,10,0

SECTION .text

GLOBAL main

main:
	mov 	eax,[a]
	push 	eax
	push 	dword chainint
	call printf
	add	esp,8
	mov 	eax,[b]
	push	eax
	push 	dword chainint
	call printf
	add	esp,8

	push chaine
        call printf
        add esp, 4

	mov	eax,0
	ret
