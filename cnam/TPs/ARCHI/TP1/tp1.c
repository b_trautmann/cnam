#include <stdio.h>
#include <stdlib.h>

void binaire(int val){
	if(val != 0){
		binaire(val/2);
		if(val%2 == 0) {
		
			printf("0");
			return;
		} else {
			printf("1");
			return;
		}
	}
	
}

void binaireFrac(float val){
	int unite = val*2.0;
	float reste = val*2.0-unite;
	printf("%d",unite);
	if(reste!=0){
		binaireFrac(reste);
	}
}

void binaireFloat(float f){

	int  integral;
	float  fractional;

	integral = (int)f;
	fractional = f - (int)f;

	binaire(integral);
	printf(".");
	binaireFrac(fractional);
}

void main() {
	float val;
	scanf("%f", &val);
	printf(" binaire de %f = ",val);
	binaireFloat(val);
	printf("\n");

}
