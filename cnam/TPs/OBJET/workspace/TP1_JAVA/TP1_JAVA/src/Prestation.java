
public class Prestation {
	
	private final String nom;
	private final double tarif;
	private final Categorie categorie;
	
	public enum Categorie {
	    PEINTURE,CARROSSERIE,MECANIQUE
	}
	
	public Prestation(String nom, double tarif, Categorie categorie){
		super();
		this.nom = nom;
		this.tarif = tarif;
		this.categorie = categorie;
	}
	
	public String toString(){
		return this.nom+" "+this.tarif+" "+this.categorie;
	}

	public String getNom() {
		return nom;
	}

	public double getTarif() {
		return tarif;
	}

	public Categorie getCategorie() {
		return categorie;
	}
}
