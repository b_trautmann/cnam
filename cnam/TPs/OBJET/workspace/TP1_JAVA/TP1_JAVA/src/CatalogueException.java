
public class CatalogueException extends Exception{
	
    public CatalogueException()
    {
       super("Impossible d'ajouter une prestation n'appartenant pas au catalogue");
    }
}
