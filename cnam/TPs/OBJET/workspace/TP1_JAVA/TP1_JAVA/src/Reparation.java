import java.util.Date;


public class Reparation{

	private static int id=0;
	private int idPrestation;
	private Date date;
	private Prestation prestation;
	
	public Prestation getPrestation() {
		return prestation;
	}

	public void setPrestation(Prestation prestation) {
		this.prestation = prestation;
	}

	Reparation(Prestation prestation, Date date) {
		this.idPrestation = Reparation.id;
		Reparation.id++;
		this.prestation = prestation;
		this.date = date;
	}

	public int getIdPrestation() {
		return idPrestation;
	}

	public void setIdPrestation(int idPrestation) {
		this.idPrestation = idPrestation;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public String toString(){
		return "Réparation numéro: "+this.idPrestation+" Date de réparation: "+this.date+"\n "+this.prestation;
	}
	
	public double getMontant(){
		return this.prestation.getTarif();
	}

}
