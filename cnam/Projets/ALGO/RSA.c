#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<gmp.h>


/* Fonction calculant les deux clés et le module de chiffrement et les stocke dans res[0], res[1] et res[2]
   
   Algorithme: 
   - Commence par déterminer si p et q sont bien premiers
   - Calcul de l'indicatrice d'euler

*/

void generer_cles(mpz_t p, mpz_t q, mpz_t *res){
	mpz_t nEul,e,d,n,inter1,inter2,start_value;
	mpz_init(nEul); mpz_init(e); mpz_init(d); mpz_init(n); mpz_init(inter1); mpz_init(inter2); mpz_init(start_value);
	
	if(est_premier(p)!=1 || est_premier(q)!=1){printf("les paramètres ne sont pas des nombres premier");return;}

	mpz_sub_ui(inter1,p,1);
	mpz_sub_ui(inter2,q,1);

	mpz_set_ui(start_value,3);  //Détermine la valeur à partir de laquelle on commence à chercher un nombre premier avec l'indicatrice d'euler

	mpz_mul(nEul, inter1, inter2); //nEul = (p-1)*(q-1)
	mpz_mul(n,p,q); 	  //n = p*q

	get_premier_proche(nEul,start_value,e);  // Récupère un nombre premier avec nEul => e la première clé

	inverse(e,nEul,d);  //Récupère l'inverse de e par nEul => d la seconde clé

	while(mpz_cmp_ui(d,0)<0){ //Si e n'a pas d'inverse valide %nEul on recommence

		mpz_add_ui(start_value,e,1);

		get_premier_proche(nEul,start_value,e);

		inverse(e,nEul,d);
	}
	
	mpz_set(res[0],n); //Stocke les résultats n et les deux clés e et d
	mpz_set(res[1],e);
	mpz_set(res[2],d);

	mpz_clear(nEul);mpz_clear(e);mpz_clear(d);mpz_clear(n);mpz_clear(inter1);mpz_clear(inter2);
	return;
}

/* Fonction permettant de crypter/décrypter un message m sous forme d'entier
   Algorithme: Transforme l'entier m en mpz_t et effectue sa puissance modulaire en utilisant la clé passé et le module de chiffrement (Clé publique + n permet de crypter et Clé privé + n permet de décrypter)
*/
long crypt( long m, mpz_t e, mpz_t n){

	mpz_t mg,res;
	mpz_init(mg);mpz_init(res);
	mpz_set_ui(mg,m);

	modpow_gmp(mg, e, n, res);

	mpz_clear(mg);

	return mpz_get_si(res);
}

