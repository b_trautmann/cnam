#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<gmp.h>

void main(char args[] ){

	printf("==============================================\n");
	printf(	"Bienvenu sur l'application de cryptage RSA ");
	printf(	"projet d'algorithmie du CNAM");
	printf(	"Par Baptiste TRAUTMANN et Quentin NIES\n");
	printf(	"==============================================");
	
	while(menu() != 0){}


	int i;
	long m;
	mpz_t p,q;
	mpz_init(p); mpz_init(q);
	mpz_set_ui(p,2767);
	mpz_set_ui(q,2851);

	m = 56;

	mpz_t res[3];
	for(i=0;i<3;i++){mpz_init(res[i]);}

	mpz_set_ui(res[0],1);

	generer_cles(p, q, res);

	printf("n = ");
	mpz_out_str(stdout,10,res[0]);

	printf(" ,e = ");
	mpz_out_str(stdout,10,res[1]);

	printf(" ,d = ");
	mpz_out_str(stdout,10,res[2]);

	printf("\n");

	printf("MESSAGE A CRYPTER = %ld\n",m);
	
	m = crypt(m, res[1],res[0]);

	printf("MESSAGE CRYPTE = %ld\n",m);

	m = crypt(m, res[2],res[0]);

	printf("MESSAGE DECRYPTE = %ld\n",m);
}

int menu() {

	return 0;
}
