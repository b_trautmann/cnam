#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <gmp.h>
#include "liste_chaine.h"

void main(char args[] )
{
	printf("==============================================\n");
	printf(	"Bienvenue sur l'application de cryptage RSA\n");
	printf(	"projet d'algorithmie du CNAM\n");
	printf(	"Par Baptiste TRAUTMANN et Quentin NIES\n");
	printf(	"==============================================\n\n\n");


	int i;
	mpz_t stockage[6];

	for(i=0;i<6;i++){mpz_init(stockage[i]);}
	
	while(menu(stockage) != 0){}
}

// Lecture d'un entier

int getint(){
	int res;
	scanf("%d",&res);
	return res;
}

/* Affichage des paramètres de cryptage */

void printParam(mpz_t * stockage){

	printf(" ========== valeurs de cryptage ========== \n");
	printf("\n               p == "); mpz_out_str(stdout,10,stockage[0]);
	printf("\n               q == "); mpz_out_str(stdout,10,stockage[1]);
	printf("\n       p x q = n == "); mpz_out_str(stdout,10,stockage[2]);
	printf("\n  Clé privée = e == "); mpz_out_str(stdout,10,stockage[3]);
	printf("\nClé publique = d == "); mpz_out_str(stdout,10,stockage[4]);
	printf("\n\n");
}

/* Saisie des paramètres de cryptage dans le tableau de mpz_t stockage organisé de cette manière :
	- stockage[0] == P (Premier nombre premer)
	- stockage[1] == Q (Second nombre premier)
	- stockage[2] == n (Module de cryptage)
	- stockage[3] == e (Clé privée)
	- stockage[4] == d (Clé publique)
*/

void remplir(mpz_t * stockage){

	int i;

	// Saisie des nombres premiers P et Q
	printf("Saisie d'un premier nombre premier : ");
	mpz_set_ui(stockage[0],getint());
	printf("p : "); mpz_out_str(stdout,10,stockage[0]);

	printf("\nSaisie d'un second nombre premier : ");
	mpz_set_ui(stockage[1],getint());
	printf("q : "); mpz_out_str(stdout,10,stockage[1]);
	printf("\n\n");

	mpz_t res[3];
	for(i=0;i<3;i++){mpz_init(res[i]);}

	// Génération des clés
	generer_cles(stockage[0], stockage[1], res);

	mpz_set(stockage[2],res[0]);
	mpz_set(stockage[3],res[1]);
	mpz_set(stockage[4],res[2]);

	// Affichage des paramètres
	printParam(stockage);

	return;
}

// Fonction de menu permettant de choisir les opérations à effectuer

int menu(mpz_t * stockage){

	char c;
	int c2;
	liste l = nouvListe();

	printf("=========  Menu  =========\n");
	printf("0  -- Saisir les paramètres de cryptage\n");
	printf("1  -- Afficher les paramètre de cryptage\n");
	printf("2  -- Saisir un message de cryptage\n");
	printf("3  -- Saisir un entier à crypter\n");
	printf("4  -- Lecture et cryptage d'un fichier\n");
	printf("5  -- Lancer une attaque\n");
	printf("-1 -- Quitter\n");
	
	scanf("%d",&c2);

	if(c2==0){
		remplir(stockage);
	} else if(c2==1){
		printParam(stockage);
	} else if(c2==2){
		cryptageMessage(stockage,l);
	} else if (c2==3){
		cryptageInt(stockage);
	} else if (c2==4){
		cryptageFile(stockage, l);
	} else if (c2==5) {
		lancementAttaque(stockage);
	}
	else{ return 0;}
	return 1;
}

