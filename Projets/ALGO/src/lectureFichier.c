#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gmp.h>
#include <liste_chaine.h>

/*Lit un fichier, et passe l'ensemble de ce qui y est écrit en ASCII dans une liste chainée donnée en paramètres */


void fileToAscii(char fichier[], liste l)
{
	int valAscii;
	int c;
	mpz_t val;
	mpz_init(val);
	FILE *file;
	file = fopen(fichier, "r");
	if (file) 
	{
	    	while ((c = getc(file)) != EOF)
	    	{
	    		//printf("%c",c);
	    		//valAscii = ;
	    		
	    		mpz_set_ui(val,c);
	    		adjq(val,l);
	    	}
	    	fclose(file);
	  	printf("\n");
	}	
}

void printFile(char fichier[]){
	char c;
	FILE *file;
	file = fopen(fichier, "r");
	if (file) 
	{
	    	while ((c = getc(file)) != EOF)
	    	{
	    		printf("%c",c);
	    	}
	   	fclose(file);
	  	printf("\n");
	}

}

void fileToListGrouped(char fichier[], liste l, int groupage)
{
	int c,i,i2;
	char car[groupage*3];
	mpz_t val;
	mpz_init(val);
	char str[3];
	
	FILE *file;
	file = fopen(fichier, "r");
	
	if(file)
	{
		i = 0;
		while ((c = getc(file)) != EOF)
	    	{
	    		
	    		if(c<10){
	    			sprintf(str,"00%d",c);
	    		} else if(c<100){
	    			sprintf(str,"0%d",c);
	    		} else {
	    			sprintf(str,"%d",c);
	    		}
	    		
	    		if(i%(groupage-1) == 0 && i != 0){ 
	    			car[i*3] = str[0];
	    			car[1+i*3] = str[1];
	    			car[2+i*3] = str[2];
	    			i = 0;
	    			mpz_set_str(val,car,10);
	    			adjq(val, l);

	    		} else {
	    			car[i*3] = str[0];
	    			car[1+i*3] = str[1];
	    			car[2+i*3] = str[2];
	    			i++;

	    		}
	    	}
	}
}

void ListToTextGrouped(liste l){

	int i;
	int i2;
	char res1[3];
	char res2[3];
	char res3[3];
	char resGmp[9];
	mpz_t val;
	mpz_init(val);
	
	int group = 0;
	
	for(i=0;i<taille(l);i++){
	
		elt(i,l,val);
		
		// RECUPERER LE GMP SOUS FORME DE CHAINE DE CARACTERES ET LE TRAITER 3 PAR 3
		/*for(i2 = 9;i>=0;i--){
	
			group++;
			if(group==3){
				
				res1[0] = elt(i);
				res1[1] = ;
				res1[2] = ;
				
			}
			if(group==6){
				res2[0] = ;
				res2[1] = ;
				res2[2] = ;
			}
			if(group==9){
				res3[0] = ;
				res3[1] = ;
				res3[2] = ;
			}
	
		}*/
	}
	

	
	
	
	 

}
