#include <stdio.h>
#include <math.h>
#include <gmp.h>

/* Détérmine si le nombre entier passé en paramètre est premier 
 * Algorithme : Exclut tous ldqzdes nombres pairs et teste tous les diviseurs
 * jusqu'à la racine carrée du paramètre et sortie de la boucle au premier
 * diviseur trouvé
 *
 * 0 == false; 1 == true 
 */

int est_premier(mpz_t nb){
	mpz_t i, limite, mod, zero, un, deux;
	int test;

	mpz_init(i); mpz_init(limite); mpz_init(mod);

	test = 1;

	mpz_sqrt(limite,nb);
	mpz_add_ui(limite,limite,1);

	mpz_mmod_ui(mod,nb,2);

	if(mpz_cmp_ui(mod,0) == 0) {				//On arrète directement si le nombre est divisible par 2
		test = 0;
	} else {
		for(mpz_set_ui(i,3); mpz_cmp(i,limite) >= 0 && !test; mpz_add_ui(i,i,2)){ //On boucle et incrément le diviseur jusqu'à que l'on trouve un diviseur ou si l'on dépasse la racine carré du nombre
			mpz_mmod(mod,nb,i);
			if(mpz_cmp_ui(mod,0) == 0){
				test = 0;
			}
		}
	}
	return test;
}

/* Renvoi un entier premier avec le paramètre inférieur à celui-ci
   Algorithme : Décrémente un compteur partant de nb-1 et détermine si celui-ci est premier avec le paramètre, si oui on arrète de boucler.
 */

void pgcd(mpz_t a, mpz_t b, mpz_t res){

	mpz_t inter; mpz_t new_res;
	mpz_init(inter); mpz_init(new_res);

	if(mpz_cmp_ui(b,0) == 0){
		mpz_set(res,a);		
	} else{
		mpz_mmod(inter,a,b);
		pgcd(b,inter,new_res);
		mpz_set(res,new_res);
	}
	return;
}

// Fonction récupérant un entier premier avec la valeur du n-Euler

void get_premier_proche(mpz_t nb, mpz_t start_value, mpz_t res) {
	mpz_t i, res_pgcd;
	mpz_init(i); mpz_init(res_pgcd);

	mpz_set(i,start_value);

	// Parcours en partant de 0 vers nb, on s'arrête au premier PGCD trouvé > 1

	pgcd(nb,i,res_pgcd);

	// Ici la recherche de PGCD n'est pas des plus idéale, car celui-ci sera toujours le plus petit possible,
	// Il faudrait imaginer une version implémentant un peu d'aléatoire ou cherchant un PGCd plus médian.

	while(mpz_cmp_ui(res_pgcd,1)>0) {
		mpz_add_ui(i,i,1);
		pgcd(nb,i,res_pgcd);
	}

	mpz_set(res,i);
	mpz_clear(i);
	mpz_clear(res_pgcd);

	return;
}

// Fonction récursive allant de paire avec la suivante pour le calcul d'inverse modulaire

void invers(mpz_t r, mpz_t u, mpz_t v, mpz_t r1, mpz_t u1, mpz_t v1, mpz_t res){

	if(mpz_cmp_ui(r1,0) == 0) mpz_set(res,u);
	else { 
		mpz_t new_res, int1, int2, int3;
		mpz_init(new_res);mpz_init(int1);mpz_init(int2);mpz_init(int3);

		mpz_div(int1,r,r1);
		mpz_mul(int1,int1,r1);
		mpz_sub(int1,r,int1);

		mpz_div(int2,r,r1);
		mpz_mul(int2,int2,u1);
		mpz_sub(int2,u,int2);

		mpz_div(int3,r,r1);
		mpz_mul(int3,int3,v1);
		mpz_sub(int3,v,int3);

		invers(r1,u1,v1,int1,int2,int3,new_res);

		mpz_set(res,new_res);

		mpz_clear(new_res);mpz_clear(int1);mpz_clear(int2);mpz_clear(int3);
	}

	return;
}

// Fonction de calcul d'inverse modulaire utilisant le théorème d'euclide étendu, implémenté avec GMP

void inverse(mpz_t p, mpz_t n, mpz_t res) //Calcul de l'inverse p modulo n et stockage dans res
{
	mpz_t a, b, c, d;

	mpz_init(a);mpz_init(b);mpz_init(c);mpz_init(d);
	mpz_set_ui(a,1);
	mpz_set_ui(b,0);
	mpz_set_ui(c,0);
	mpz_set_ui(d,1);
	invers(p,a,b,n,c,d,res);

	mpz_clear(a);mpz_clear(b);mpz_clear(c);mpz_clear(d);	

	return;
}

// Calcul d'éxponentielle modulaire pour un long

long modpow(long base, long exp, long m){
	long result = 1;
	while(exp > 0){
		if(exp & 1 > 0){
			result = (result * base)%m;
		}
		exp >>= 1;
		base = (base * base)%m;
	}
	return result;
}

// Fonction de calcul d'éxponentielle modulaire avec GMP, trop lourde à utiliser donc obsolète

void modpow_gmp(mpz_t base, mpz_t expo, mpz_t m, mpz_t res){

	mpz_t exp;
	mpz_init(exp);
	mpz_set(exp,expo);
	mpz_set_ui(res,1);
	while(mpz_cmp_ui(exp,0)>0){
		mpz_mul(res,res,base);
		mpz_mmod(res,res,m);

		mpz_sub_ui(exp,exp,1);
	}
}
