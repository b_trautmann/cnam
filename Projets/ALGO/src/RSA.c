#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <gmp.h>
#include "liste_chaine.h"

// Fonction permettant de générer des clés en passant deux nombres premiers

void generer_cles(mpz_t p, mpz_t q, mpz_t *res){

	mpz_t nEul,e,d,n,inter1,inter2,start_value;
	mpz_init(nEul); mpz_init(e); mpz_init(d); mpz_init(n); mpz_init(inter1); mpz_init(inter2); mpz_init(start_value);
	
	if(est_premier(p)!=1 || est_premier(q)!=1){printf("les paramètres ne sont pas des nombres premier");return;}

	// Calcul du n-Euler
	mpz_sub_ui(inter1,p,1);
	mpz_sub_ui(inter2,q,1);

	mpz_set_ui(start_value,3);

	mpz_mul(nEul, inter1, inter2);

	// Calcul du module de chiffrement
	mpz_mul(n,p,q);

	// Recherche d'un nombre premier avec le n-Euler
	get_premier_proche(nEul,start_value,e);

	// Calcul de l'inverse modulaire, soit l'autre clé de la paire
	inverse(e,nEul,d);

	// Il s'avère qu'avec ce système l'inverse calculé n'est pas toujours valide (valeurs négative), le problème a donc été contourné en relançant la recherche de PGCD

	while(mpz_cmp_ui(d,0)<0){ //Si e n'a pas d'inverse valide %nEul on recommence jusqu'a obtenir une clé inverse valide

		mpz_add_ui(start_value,e,1);

		get_premier_proche(nEul,start_value,e);

		inverse(e,nEul,d);

	}
	
	//Stockage des résultats dans la variable résultat
	mpz_set(res[0],n);
	mpz_set(res[1],e);
	mpz_set(res[2],d);

	mpz_clear(nEul);mpz_clear(e);mpz_clear(d);mpz_clear(n);mpz_clear(inter1);mpz_clear(inter2);
	return;
}

// Cryptage / Décryptage avec m le message, e la clé de cryptage / décryptage, n le module de chiffrement et retour le résultat de la fonction

void crypt( mpz_t m, mpz_t e, mpz_t n, mpz_t retour){

	mpz_t mg,res;
	mpz_init(mg);mpz_init(res);
	mpz_set(mg,m);

	//modpow_gmp(mg, e, n, res); Ancienne version utilisant une fonction faite main non optimisée
	mpz_powm(res, mg,e,n); // Calcul de l'éxponentielle modulaire

	mpz_clear(mg);

	mpz_set_ui(retour,mpz_get_si(res));
}

// Fonction lançant l'attaque avec saisie d'un entier comme message à tenter de décrypter

void lancementAttaque(mpz_t * stockage){

	mpz_t test; mpz_init(test);
	mpz_set_ui(test,getint());
	mpz_t testCrypted; mpz_init(testCrypted);
	crypt(test, stockage[3],stockage[2],testCrypted); // Calcul de la version cryptée du message
	printf("On cherche à retrouver le message : \"");
	mpz_out_str(stdout,10,test);
	printf("\" à partir de sa version crypté : \"");
	mpz_out_str(stdout,10,testCrypted);
	printf("\"\n");
	attaquer(stockage[3], stockage[2], testCrypted , test); // Lancement de l'attaque en passant le module de chiffrement, la clé publique, le message crypté et le résultat recherché
}

// Cryptage champ par champ d'une liste

liste cryptageListe(liste l, mpz_t * stockage)
{
	int i;
	mpz_t courant, crypte;
	mpz_init(courant);
	mpz_init(crypte);
	liste l2 = nouvListe();
	for(i=0;i < taille(l); ++i) { // Parcours de la liste de sorte à crypter chaque champ et à remplir une nouvelle liste
		elt(i,l,courant);
		crypt(courant, stockage[3],stockage[2],crypte);
		adjq(crypte,l2);
	}
	return l2;

}

// Décryptage champs par champs d'une liste 

liste deCryptageListe(liste l, mpz_t * stockage)
{
	int i;
	mpz_t courant, decrypte;
	mpz_init(courant);
	mpz_init(decrypte);
	liste l2 = nouvListe();
	for(i=0;i < taille(l); ++i) { // Parcours de la liste à décrypter chaque champ et à remplir une nouvelle liste
		elt(i,l,courant);
		crypt(courant, stockage[4],stockage[2],decrypte);
		adjq(decrypte,l2);
	}
	return l2;

}

/* Cryptage d'un entier saisie au clavier */

void cryptageMessage(mpz_t * stockage, liste l){

	mpz_t m;
	mpz_init(m);
	char saisie[300];
	int i = 0;
	char str[3];

	printf("\nSaisie d'un message : ");
	scanf("%s",saisie);

	while (saisie[i] != '\0') // Parcours caractère par caractère du fichier
	{
		// Si on a lut le nombre de caractères attendu on transforme la chaine sous forme de MPZ afin de pouvoir le crypter

		sprintf(str,"%d",saisie[i]);
		mpz_set_str(m,str,10);

		adjq(m, l);

		i++;

	}

	afficheListe(l);
	liste lCryptee;
	lCryptee = cryptageListe(l,stockage);
	printf("\nVersion cryptée\n");
	afficheListe(lCryptee); // Affichage de la liste cryptée
	lCryptee = deCryptageListe(lCryptee,stockage);
	printf("\nVersion décryptée\n");
	afficheListe(lCryptee); // Affichage de la liste décryptée
	printf("\n\n");
}

// Cryptage d'une chaine de caractère contenue dans un fichier de sorte à produire et à afficher une liste contenant les codes ASCII cryptés et décryptés

void cryptageFile(mpz_t * stockage, liste l){

	fileToListGrouped("aLire.txt",l,3);  // Fonction générant la liste contenant des mpz_t des codes ASCII regroupés 3 par 3
	printf("\nTexte à crypter contenu dans le fichier\n");
	printFile("aLire.txt"); // Affichage du contenu du fichier
	printf("\nContenu de la liste avec les codes ASCII regroupés : \n");
	afficheListe(l);
	liste lCryptee;
	lCryptee = cryptageListe(l,stockage);
	printf("\nVersion cryptée\n");
	afficheListe(lCryptee); // Affichage de la liste cryptée
	lCryptee = deCryptageListe(lCryptee,stockage);
	printf("\nVersion décryptée\n");
	afficheListe(lCryptee); // Affichage de la liste décryptée
	printf("\n");
}

// Cryptage simple d'un entier saisi

void cryptageInt(mpz_t * stockage){

	printf("\nVeuillez saisir un entier : ");
	int m = getint();

	mpz_t message;
	mpz_init(message);
	mpz_set_ui(message,m);
	
	crypt(message, stockage[3],stockage[2],message);

	printf("Message crypté = ");
	mpz_out_str(stdout,10,message);
	printf("\n");

	crypt(message, stockage[4],stockage[2],message);

	printf("Message décrypté = ");
	mpz_out_str(stdout,10,message);
	printf("\n\n");

}