#include<stdio.h>
#include<math.h>
#include<gmp.h>

/*  Fonction permettant de simuler une attaque bruteforce pour décourvrir la clé inverse de la clé passée
	Son fonctionnement se fait de sorte que des diviseurs sont testés pour n de manière décrémentale.
	Ainsi si des diviseurs possibles sont découverts pour le module de chiffrement on réeffectue un calcul de la clé privée et on teste si le décryptage renvoit le message attendu
*/

void attaquer(mpz_t cle, mpz_t n, mpz_t messageCrypt, mpz_t recherche){


	mpz_t i,cle_test,q,cle_inverse,inter,nEul,test,res;

	// Initialisation de tous les paramètres nécessaires
	mpz_init(i);mpz_init(cle_test);mpz_init(test);mpz_init(cle_inverse);mpz_init(inter);mpz_init(nEul);mpz_init(res);mpz_init(q);

	printf("clé publique passée : ");mpz_out_str(stdout,10,cle);printf("\n");

	mpz_root(i,n,1);

	for(mpz_sub_ui(i,i,1);mpz_cmp_ui(i,0)>0;mpz_sub_ui(i,i,1)){ // Parcours de i = n jusqu'à 0
		mpz_mod(test,n,i);
		if(mpz_cmp_ui(test,0)==0){ // Si la division est entière

			// On calcul le nEuler correspondant au p et q trouvés
			mpz_div(nEul,n,i);
			mpz_div(q,n,i);
			mpz_sub_ui(nEul,nEul,1);
			mpz_sub_ui(inter,i,1);
			mpz_mul(nEul,nEul,inter);

			// Affichage de la supposition faite
			printf("====== Suppositions de paramètres possibles :\n");
			printf("p == ");mpz_out_str(stdout,10,i);printf("\n");
			printf("q == ");mpz_out_str(stdout,10,q);printf("\n");
			printf("nEul == ");mpz_out_str(stdout,10,nEul);printf("\n");

			inverse(cle,nEul,cle_inverse); //Calcul de la clé inverse (clé privée)

			// Clé privée supposée
			printf("clé privée == ");mpz_out_str(stdout,10,cle_inverse);printf("\n\n");

			// Décryptage du message avec les paramètres supposés
			crypt(messageCrypt,cle_inverse,n,res);

			// Affichage du résultat et du test
			printf("On test si le message decrypté est le bon \n");
			printf("Test de : ");
			mpz_out_str(stdout,10,recherche);
			printf(" == ");mpz_out_str(stdout,10,res);
			printf("\n");

			if(mpz_cmp(res,recherche)==0){ // Si le résultat est trouvé on termine la recherche
				printf("La clé privé trouvée est : ");
				mpz_out_str(stdout,10,cle_inverse);
				printf(" !\n");	
			
				return;
			} else { //Sinon on continue
				printf("Non concluant, on continue à chercher...\n\n");
			}
		}
	}

	return;
}
