#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include "liste_chaine.h"


/** liste vide
 *  precond : - 
 */

liste nouvListe() {
    liste l = (liste)malloc(sizeof(struct LISTE));
        
    l->premier = NULL;

    return l;
}

/** Ajout d'un élément en tête de la liste
 *  precond : - 
 *  entrée : x l'élément à ajouter, l la liste
 *  retour : - (on travaille dans la même liste)
 */

void adjt(mpz_t x, liste l) {
	elt_liste enouv = (elt_liste)malloc(sizeof(struct ELT));
	
	mpz_set(enouv->val,x);
	enouv->suiv = l->premier;
	l->premier = enouv;
	
	return;
}
	
/** Ajout d'un élément en fin  de  liste
 *  precond : - 
 *  retour : - (attention : on travaille dans la même liste)
 */

void adjq(mpz_t x, liste l) {
	//la valeur x est initialisée dans la fct qui fait appel a la fct
	elt_liste enouv = (elt_liste)malloc(sizeof(struct ELT));
	mpz_init(enouv->val);
	enouv->suiv = NULL;
	elt_liste cour = l->premier;
	// initialisation de la nouvelle cellule
	mpz_set(enouv->val,x);
	
	// si aucun élément
	
	if (cour == NULL) { l->premier = enouv;  }
	else {
		// sinon recherche du dernier
		while (cour->suiv != NULL) {
			cour = cour->suiv;
			
		}
		// ajout en fin
		cour->suiv = enouv;
	}
	//printf("Ca passe.");
}

/** Taille d'une liste
 *  precond : - 
 *  retour : le nombre d'éléments présents dans la liste
 */


int taille(liste l) {
	elt_liste cour = l->premier;
	int t=0;
	while (cour) { 
		t++;
		cour = cour->suiv;
	} 
	return t;
}

/** Ajout d'un élément dans une liste à la place ind
 *  precond : 0 <= ind < taille(l)
 *  retour : - (attention : on travaille dans la même liste)
 */

void insert(mpz_t e, liste l, int ind) {
    	elt_liste enouv = (elt_liste)malloc(sizeof(struct ELT));
	elt_liste prec = NULL, cour = l->premier;
	int idx;
	idx = ind;
	mpz_set(enouv->val, e);
	// e sera insere entre les celulles prev et cour
	while (idx > 0) {
		prec = cour;
		cour=cour->suiv;
		--idx;
		
	}
	
	enouv->suiv = cour;
	if (prec != NULL) { prec->suiv = enouv; }
	else { l->premier = enouv; }
	
    return;    
}

/** Renvoie l'element en tete de liste
 *  precond : l existe && taille(l) >= 1
 *  retour : l'élément
 */

void tete(liste l, mpz_t t) { mpz_set(t,l->premier->val); }

/** Renvoie l'element en fin de liste
 *  precond : l existe && taille(l) >= 1
 *  retour : l'élément
 */

void queue(liste l, mpz_t t) { 
	elt_liste cour = l->premier;
	while (cour->suiv != NULL) { 
		cour = cour->suiv;
	} 
	mpz_set(t,cour->val);
}



/** Suppression en tête (aucun effet si la liste est vide)
 *  precond : - 
 *  retour : - (attention : on travaille dans la même liste)
 */

void supt(liste l) {
    if (l->premier == NULL) { return; }
    
    elt_liste e = l->premier;
    l->premier = e->suiv;
    free(e);

	return;
}

/** Suppression en fin (aucun effet si la liste est vide)
 *  precond : - 
 *  retour : - (attention : on travaille dans la même liste)
 */

void supq(liste l) {
    elt_liste cour = l->premier;
	
	// si un seul element
	if (cour->suiv == NULL) { 
		l->premier = NULL;
		free(cour);
	} else {
		// recherche de l'avant-dernier
		while (cour->suiv->suiv != NULL) { 
			cour = cour->suiv; 
		}
		free(cour->suiv);
		cour->suiv = NULL;
	}
	return;
}

/** Test de vacuité d'une liste
 *  precond : - 
 *  retour : 1 si vide, 0 sinon
 */

int estVide(liste l) {
	return (l->premier != NULL?0:1);
}

/** rend l'element à la place i
 *  precond : 0 <= i < taille(l)
 *  retour : l'element
 */

void elt(int i, liste l, mpz_t t) {
    int ind = i;
    elt_liste cour = l->premier;
    
    while(i > 0) { cour = cour->suiv; --i; }
    mpz_init(t);
    mpz_set(t,cour->val);
}


/** Suppression de tous les éléments d'une liste
 *  precond : - 
 */
 
void afficheListe(liste l) {
	int i;
	elt_liste cour = l->premier, suiv;
	
	for(i=0; i < taille(l); ++i) {
		mpz_out_str(stdout,10,cour->val);
		printf(" ");
		suiv = cour->suiv;
		cour=suiv;
	}
	printf("\n");
}

// Désallocation de la liste

static void libere_liste(liste l) {
	elt_liste cour = l->premier, suiv;
	
	while (cour) {
		suiv = cour->suiv;
		free(cour);
		cour = suiv;
	}
}

void fermeListe(liste l) {
	libere_liste(l);
    free(l);
}
