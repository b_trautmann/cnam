#ifndef __LISTE_CHAINE_H
#define __LISTE_CHAINE_H
#include <gmp.h>

typedef struct ELT {
   mpz_t val;
   struct ELT *suiv;
} *elt_liste;

typedef struct LISTE {
   elt_liste premier;
} *liste;

liste nouvListe();		// nouvelle liste
void adjt(mpz_t, liste);   	// adjonction en tête
void adjq(mpz_t, liste);	// adjonction en queue
int taille(liste); 	        // rend la taille de la liste

void insert(mpz_t, liste, int); // insertion du mpz_t dans l a la place ind
void tete(liste, mpz_t);        // met la tête de la liste dans t
void queue(liste, mpz_t); 	// met la fin de la liste dans t
void supt(liste);	       	// suppression en tête
void supq(liste);        	// suppression en queue
int estVide(liste);    		// 1 liste vide, 0 sinon
void elt(int, liste, mpz_t);    // rend un élément (0 <= ind < taille(l))
void afficheListe(liste);	//affiche le contenu de la liste
void fermeListe(liste);		// clos la liste

#endif
