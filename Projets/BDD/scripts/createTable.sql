SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

USE `asoiaf` ;

-- -----------------------------------------------------
-- Table `asoiaf`.`VILLE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `asoiaf`.`VILLE` ;

CREATE  TABLE IF NOT EXISTS `asoiaf`.`VILLE` (
  `idVille` INT NOT NULL ,
  `nom` VARCHAR(45) NOT NULL ,
  `territoire` INT NOT NULL ,
  PRIMARY KEY (`idVille`) ,
  INDEX `fk_VILLE_TERRITOIRE1` (`territoire` ASC) ,
  CONSTRAINT `fk_VILLE_TERRITOIRE1`
    FOREIGN KEY (`territoire` )
    REFERENCES `asoiaf`.`TERRITOIRE` (`idTerritoire` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `asoiaf`.`TERRITOIRE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `asoiaf`.`TERRITOIRE` ;

CREATE  TABLE IF NOT EXISTS `asoiaf`.`TERRITOIRE` (
  `idTerritoire` INT NOT NULL ,
  `nom` VARCHAR(45) NOT NULL ,
  `capitale` INT NOT NULL ,
  PRIMARY KEY (`idTerritoire`) ,
  INDEX `fk_TERRITOIRE_VILLE1` (`capitale` ASC) ,
  CONSTRAINT `fk_TERRITOIRE_VILLE1`
    FOREIGN KEY (`capitale` )
    REFERENCES `asoiaf`.`VILLE` (`idVille` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `asoiaf`.`MAISON`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `asoiaf`.`MAISON` ;

CREATE  TABLE IF NOT EXISTS `asoiaf`.`MAISON` (
  `idMaison` INT NOT NULL ,
  `nom` VARCHAR(45) NOT NULL ,
  `territoire` INT NOT NULL ,
  `suzerain` INT NULL ,
  PRIMARY KEY (`idMaison`) ,
  INDEX `fk_MAISON_TERRITOIRE1` (`territoire` ASC) ,
  INDEX `fk_MAISON_MAISON1` (`suzerain` ASC) ,
  CONSTRAINT `fk_MAISON_TERRITOIRE1`
    FOREIGN KEY (`territoire` )
    REFERENCES `asoiaf`.`TERRITOIRE` (`idTerritoire` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_MAISON_MAISON1`
    FOREIGN KEY (`suzerain` )
    REFERENCES `asoiaf`.`MAISON` (`idMaison` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `asoiaf`.`PERSONNAGE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `asoiaf`.`PERSONNAGE` ;

CREATE  TABLE IF NOT EXISTS `asoiaf`.`PERSONNAGE` (
  `idPersonnage` INT NOT NULL ,
  `Prenom` VARCHAR(45) NOT NULL ,
  `maison` INT NULL ,
  `pere` INT NULL ,
  `mere` INT NULL ,
  `epoux` INT NULL ,
  `sexe` CHAR NOT NULL ,
  `dateNaissance` DATE NOT NULL ,
  `dateMort` DATE NULL ,
  `titre` VARCHAR(45) NULL ,
  `Culture` VARCHAR(45) NULL ,
  PRIMARY KEY (`idPersonnage`) ,
  INDEX `fk_PERSONNAGE_MAISON` (`maison` ASC) ,
  INDEX `fk_PERSONNAGE_PERSONNAGE1` (`pere` ASC) ,
  INDEX `fk_PERSONNAGE_PERSONNAGE2` (`mere` ASC) ,
  INDEX `fk_PERSONNAGE_PERSONNAGE3` (`epoux` ASC) ,
  CONSTRAINT `fk_PERSONNAGE_MAISON`
    FOREIGN KEY (`maison` )
    REFERENCES `asoiaf`.`MAISON` (`idMaison` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PERSONNAGE_PERSONNAGE1`
    FOREIGN KEY (`pere` )
    REFERENCES `asoiaf`.`PERSONNAGE` (`idPersonnage` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PERSONNAGE_PERSONNAGE2`
    FOREIGN KEY (`mere` )
    REFERENCES `asoiaf`.`PERSONNAGE` (`idPersonnage` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PERSONNAGE_PERSONNAGE3`
    FOREIGN KEY (`epoux` )
    REFERENCES `asoiaf`.`PERSONNAGE` (`idPersonnage` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `asoiaf`.`TYPEEVENEMENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `asoiaf`.`TYPEEVENEMENT` ;

CREATE  TABLE IF NOT EXISTS `asoiaf`.`TYPEEVENEMENT` (
  `codeType` INT NOT NULL ,
  `descriptionType` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`codeType`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `asoiaf`.`EVENEMENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `asoiaf`.`EVENEMENT` ;

CREATE  TABLE IF NOT EXISTS `asoiaf`.`EVENEMENT` (
  `idEvenement` INT NOT NULL ,
  `intitule` VARCHAR(45) NOT NULL ,
  `territoire` INT NOT NULL ,
  `date` INT NOT NULL ,
  `typeEvenement` INT NULL ,
  PRIMARY KEY (`idEvenement`) ,
  INDEX `fk_EVENEMENT_TERRITOIRE1` (`territoire` ASC) ,
  INDEX `fk_EVENEMENT_TYPEEVENEMENT1` (`typeEvenement` ASC) ,
  CONSTRAINT `fk_EVENEMENT_TERRITOIRE1`
    FOREIGN KEY (`territoire` )
    REFERENCES `asoiaf`.`TERRITOIRE` (`idTerritoire` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_EVENEMENT_TYPEEVENEMENT1`
    FOREIGN KEY (`typeEvenement` )
    REFERENCES `asoiaf`.`TYPEEVENEMENT` (`codeType` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `asoiaf`.`ESTPRESENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `asoiaf`.`ESTPRESENT` ;

CREATE  TABLE IF NOT EXISTS `asoiaf`.`ESTPRESENT` (
  `evenement` INT NULL ,
  `personnage` INT NULL ,
  INDEX `fk_ESTPRESENT_PERSONNAGE1` (`personnage` ASC) ,
  INDEX `fk_ESTPRESENT_EVENEMENT1` (`evenement` ASC) ,
  PRIMARY KEY (`evenement`, `personnage`) ,
  CONSTRAINT `fk_ESTPRESENT_PERSONNAGE1`
    FOREIGN KEY (`personnage` )
    REFERENCES `asoiaf`.`PERSONNAGE` (`idPersonnage` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ESTPRESENT_EVENEMENT1`
    FOREIGN KEY (`evenement` )
    REFERENCES `asoiaf`.`EVENEMENT` (`idEvenement` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `asoiaf`.`TYPEORGANISATION`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `asoiaf`.`TYPEORGANISATION` ;

CREATE  TABLE IF NOT EXISTS `asoiaf`.`TYPEORGANISATION` (
  `idTypeOrganisation` INT NOT NULL ,
  `Intitule` VARCHAR(45) NULL ,
  PRIMARY KEY (`idTypeOrganisation`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `asoiaf`.`ORGANISATION`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `asoiaf`.`ORGANISATION` ;

CREATE  TABLE IF NOT EXISTS `asoiaf`.`ORGANISATION` (
  `idOrganisation` INT NOT NULL ,
  `nom` VARCHAR(45) NOT NULL ,
  `typeOrganisation` INT NOT NULL ,
  PRIMARY KEY (`idOrganisation`) ,
  INDEX `fk_ORGANISATION_TYPEORGANISATION1` (`typeOrganisation` ASC) ,
  CONSTRAINT `fk_ORGANISATION_TYPEORGANISATION1`
    FOREIGN KEY (`typeOrganisation` )
    REFERENCES `asoiaf`.`TYPEORGANISATION` (`idTypeOrganisation` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `asoiaf`.`FAITPARTIE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `asoiaf`.`FAITPARTIE` ;

CREATE  TABLE IF NOT EXISTS `asoiaf`.`FAITPARTIE` (
  `organisation` INT NULL ,
  `personnage` INT NOT NULL ,
  INDEX `fk_FAITPARTIE_PERSONNAGE1` (`personnage` ASC) ,
  INDEX `fk_FAITPARTIE_ORGANISATION1` (`organisation` ASC) ,
  PRIMARY KEY (`organisation`, `personnage`) ,
  CONSTRAINT `fk_FAITPARTIE_PERSONNAGE1`
    FOREIGN KEY (`personnage` )
    REFERENCES `asoiaf`.`PERSONNAGE` (`idPersonnage` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FAITPARTIE_ORGANISATION1`
    FOREIGN KEY (`organisation` )
    REFERENCES `asoiaf`.`ORGANISATION` (`idOrganisation` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
