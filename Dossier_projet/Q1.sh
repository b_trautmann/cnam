#/bin/bashll

Deplac(){
	pwd
	PS3='Souhaitez vous vous déplacer ?'
	options=("Oui" "Non")
	select opt in "${options[@]}"
	do
	    case $opt in
	    	"Oui")
				echo "Dans le répertoire courant:"
				ls
				#echo "Dans le répertoire parent:"
				#ls  ..
				echo "Veuillez saisir un nom de répertoire cible"
				read name
				cd $name
				Deplac
				break
				;;
			"Non")
				
				break
				;;
			*)
				break;;
		esac
	done
}

scriptFind="$(pwd)/find_sh.sh"
scriptMkdir="$(pwd)/mkdir_replace_sh.sh"

Deplac

bash $scriptFind "$(pwd)"
bash $scriptMkdir "$(pwd)"
