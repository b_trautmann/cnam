#!/bin/bash

POS_NEG () { 

	res="$1"
	PS3='Veuillez choisir une condition à ajouter : '
	optionsC=("Positive" "Négative")

	select optC in "${optionsC[@]}"
	do
		case $optC in
			"Positive")
				res="$res '*${2}*'"
				;;
			"Négative")
				res="$res ! '*${2}*'"
				;;
			*) ;;
		esac
		break
	done
	echo "$res"
	return 0
}

cexec="egrep "

echo -e "######     Proposition de code labos à rechercher     ######\n"

ls ./alias/* | sed 's/^\.\/alias\///g' | sed 's/\.php//g' | sed 's/[-_].*//g' | uniq | tr '\n' '\t'


echo -e "\n\nVeuillez saisir la chaine de recherche : "
read c1

cexec=$(POS_NEG "$cexec" $c1)

if [[ -z "$1" ]]
then
	cexec="$cexec -r $(pwd)/*"
else
	cexec="$cexec -r $1/*"
fi


PS3='Ajouter une condition ? : '
optionsC=("ET" "OU" "STOP")
select optC in "${optionsC[@]}"
do
	case $optC in
		"ET")
			echo -e "\n\nVeuillez saisir la chaine de recherche : "
			read c1
			cexec="$cexec -a "
			cexec=$(POS_NEG "$cexec" $c1)
			;;
		"OU")
			echo -e "\n\nVeuillez saisir la chaine de recherche : "
			read c1
			cexec="$cexec -o"
			cexec=$(POS_NEG "$cexec" $c1)
			;;
		"STOP")
			break
			;;
		*) break;;
	esac
done
#echo -e "\n\nRésultats :"
#eval $cexec

echo "$cexec"

eval $cexec -l > res_find
eval $cexec -n > res_print
echo "Résultats : "

more res_print

PS3='remplacer chaine de caractère, ajouter une ligne ou en supprimer ? : '
optionsC=("Remplacer chaine de caractères" "insérer une ligne" "supprimer une ligne" "STOP")
select optC in "${optionsC[@]}"
do
	case $optC in
		"Remplacer chaine de caractères")
			echo -e "\n\nVeuillez saisir la chaine à remplacer : "
			read c1
			echo -e "\n\nVeuillez saisir la chaine de remplacement : "
			read c2
			while read p; do
				echo "sed -i 's/$c1/$c2/g' $p"
				sed -i 's/$c1/$c2/g' $p
			done <res_find
			#echo res_find | xargs -L1 sed 's/$c1/$c2/g'
			;;
		"insérer une ligne")
			echo -e "\n\nVeuillez saisir le numéro de la ligne à insérer : "
			read line
			echo -e "\n\nVeuillez saisir la ligne à insérer : "
			read c2
			while read p; do
				sed -i "${line}i $c2" $p
			done <res_find
			;;
		"supprimer une ligne")
			echo -e "\n\nVeuillez saisir le numéro de la ligne à supprimer : "
			read line
			while read p; do
				sed -i "${line}d" $p
			done <res_find
			break
			;;
		"STOP")
			break
			;;
		*) break;;
	esac
done