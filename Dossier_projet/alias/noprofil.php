<?php
include_once '/usr/local/lib/arhi/auth.php';

$mysqli = connect();

if( $result = $mysqli->query(
			     "
SELECT m0_.aliasEmail
FROM 
  members m0_ 
  LEFT JOIN inteam i3_ ON m0_.id = i3_.member_id 
  LEFT JOIN team t1_ ON i3_.team_id = t1_.id 
  LEFT JOIN trainee t4_ ON m0_.id = t4_.member_id 
  LEFT JOIN doctorant d5_ ON m0_.id = d5_.member_id 
  LEFT JOIN cotutelle c6_ ON d5_.id = c6_.doctorant_id 
  LEFT JOIN ita i7_ ON m0_.id = i7_.member_id 
  LEFT JOIN cec c8_ ON m0_.id = c8_.member_id 
  LEFT JOIN postdoc p9_ ON m0_.id = p9_.member_id 
  LEFT JOIN office o2_ ON m0_.id = o2_.member_id 
WHERE 
  m0_.locked = 0 
  AND m0_.enabled = 1 
  AND t4_.id IS NULL 
  AND c6_.id IS NULL 
  AND i7_.id IS NULL 
  AND c8_.id IS NULL 
  AND p9_.id IS NULL 
  AND d5_.id IS NULL 
GROUP BY m0_.id
ORDER BY 
  m0_.lastName ASC", MYSQLI_USE_RESULT) ){

  while( $row = $result->fetch_row() ){
    printf("%s\n", $row[0]);
  }
  
  $result->close();
}

if( $result = $mysqli->query(
			     "
SELECT m0_.aliasEmail
FROM 
  members m0_ 
  LEFT JOIN inteam i3_ ON m0_.id = i3_.member_id 
  LEFT JOIN team t1_ ON i3_.team_id = t1_.id 
  LEFT JOIN trainee t4_ ON m0_.id = t4_.member_id 
  LEFT JOIN doctorant d5_ ON m0_.id = d5_.member_id 
  LEFT JOIN cotutelle c6_ ON d5_.id = c6_.doctorant_id 
  LEFT JOIN office o2_ ON m0_.id = o2_.member_id 
WHERE 
  m0_.locked = 0 
  AND m0_.enabled = 1 
  AND t4_.id IS NULL 
  AND c6_.id IS NULL 
  AND (
    o2_.id IS NULL 
    OR o2_.phone IS NULL
  ) 
GROUP BY m0_.id
ORDER BY 
  m0_.lastName ASC", MYSQLI_USE_RESULT) ){

  while( $row = $result->fetch_row() ){
    printf("%s\n", $row[0]);
  }
  
  $result->close();
}


$mysqli->close();