<?php
include_once '/usr/local/lib/arhi/auth.php';

$mysqli = connect();

if( $result = $mysqli->query("SELECT aliasEmail FROM members, hdr, member_tag
                              WHERE locked=0 AND ((member.hdr_id is NOT NULL AND members.hdr_id=hdr.id AND hdr.doctoralSchool_id=2) OR (member.id=member_tag.member_id AND member_tag.tag_id=4))
                               ORDER BY aliasEmail", MYSQLI_USE_RESULT) ){

  while( $row = $result->fetch_row() ){
    printf("%s\n", $row[0]);
  }
  
  $result->close();
}

$mysqli->close();
