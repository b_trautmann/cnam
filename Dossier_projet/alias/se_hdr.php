<?php
include_once '/usr/local/lib/arhi/auth.php';

$mysqli = connect();

if( $result = $mysqli->query("SELECT aliasEmail FROM members, inteam
                              WHERE locked=0 AND members.hdr_id IS NOT NULL
                              AND members.id NOT IN (SELECT member_id FROM inteam) GROUP BY aliasEmail", MYSQLI_USE_RESULT) ){

  while( $row = $result->fetch_row() ){
    printf("%s\n", $row[0]);
  }
  
  $result->close();
}

$mysqli->close();
