<?php
include_once '/usr/local/lib/arhi/auth.php';

$mysqli = connect();

if( $result = $mysqli->query("SELECT aliasEmail FROM members
                              WHERE locked=0 AND commonService_id IN(1, 2, 3, 4) GROUP BY aliasEmail", MYSQLI_USE_RESULT) ){

  while( $row = $result->fetch_row() ){
    printf("%s\n", $row[0]);
  }
  
  $result->close();
}

$mysqli->close();