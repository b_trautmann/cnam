<?php
include_once '/usr/local/lib/arhi/auth.php';

$mysqli = connect();

if( $result = $mysqli->query("SELECT aliasEmail FROM members, doctorant
                              WHERE locked=0 AND members.id=doctorant.member_id AND doctoralSchool_id=1
                              AND ((doctorant.endDate IS NULL OR doctorant.endDate > now()) AND doctorant.startDate < now())", MYSQLI_USE_RESULT) ){

  while( $row = $result->fetch_row() ){
    printf("%s\n", $row[0]);
  }
  
  $result->close();
}

$mysqli->close();
