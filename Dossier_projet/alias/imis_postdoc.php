<?php
include_once '/usr/local/lib/arhi/auth.php';

$mysqli = connect();

if( $result = $mysqli->query("SELECT aliasEmail FROM members, inteam, postdoc
                              WHERE locked=0 AND members.id=inteam.member_id AND members.id=postdoc.member_id AND team_id=10
                              AND ((postdoc.endDate IS NULL OR postdoc.endDate > now()) AND postdoc.startDate < now())", MYSQLI_USE_RESULT) ){

  while( $row = $result->fetch_row() ){
    printf("%s\n", $row[0]);
  }
  
  $result->close();
}

$mysqli->close();
