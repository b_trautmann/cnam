<?php
include_once '/usr/local/lib/arhi/auth.php';

$mysqli = connect();

if( $result = $mysqli->query("SELECT aliasEmail FROM members, inteam, postdoc, member_tag
                              WHERE locked=0 AND members.id=inteam.member_id AND members.id=postdoc.member_id AND members.id=member_tag.member_id AND member_tag.tag_id=2 AND team_id=7
                              AND ((postdoc.endDate IS NULL OR postdoc.endDate > now()) AND postdoc.startDate < now())", MYSQLI_USE_RESULT) ){

  while( $row = $result->fetch_row() ){
    printf("%s\n", $row[0]);
  }
  
  $result->close();
}

$mysqli->close();
