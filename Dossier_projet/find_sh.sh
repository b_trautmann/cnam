#!/bin/bash


POS_NEG () { 

	res="$1"
	PS3='Veuillez choisir une condition à ajouter : '
	optionsC=("Positive" "Négative")

	select optC in "${optionsC[@]}"
	do
		case $optC in
			"Positive")
				res="$res -name \"*${2}*\""
				;;
			"Négative")
				res="$res ! -name \"*${2}*\""
				;;
			*) ;;
		esac
		break
	done
	echo "$res"
	return 0
}

if [[ -z "$1" ]]
then
	cexec="find \"$(pwd)\""
else
	cexec="find \"$1\""
fi

PS3='Veuillez choisir une méthode de recherche : '
options=("Recherche simple" "Recherche simple inverse" "Recherche simple multiple" "Recherche complexe" "Sortir")
select opt in "${options[@]}"
do
    case $opt in
        "Recherche simple")
			echo "Critère de recherche : "
            read c1
            cexec="$cexec -name \"*${c1}*\""

			eval $cexec > res_find
			echo "Nombre de lignes récupérées par la recherche : "
			wc -l res_find
			path="$(pwd)"
            ;;
        "Recherche simple inverse")
            read c1
            cexec="$cexec ! -name \"*${c1}*\""

            
			eval $cexec > res_find
			echo "Nombre de lignes récupérées par la recherche : "
			wc -l res_find
            ;;
        "Recherche simple multiple")
			read c1

			for cond in $c1
			do
				cexec="$cexec -name \"*${cond}*\" -o"
			done
			cexec=${cexec::-2}


			eval $cexec > res_find
			echo "Nombre de lignes récupérées par la recherche : "
			wc -l res_find
            ;;
        "Recherche complexe")



			echo -e "\n\nVeuillez saisir la chaine de recherche : "
			read c1

			cexec=$(POS_NEG "$cexec" $c1)


			PS3='Ajouter une condition ? : '
			optionsC=("ET" "OU" "STOP")
			select optC in "${optionsC[@]}"
			do
				case $optC in
					"ET")
						echo -e "\n\nVeuillez saisir la chaine de recherche : "
						read c1
						cexec="$cexec -a "
						cexec=$(POS_NEG "$cexec" $c1)
						;;
					"OU")
						echo -e "\n\nVeuillez saisir la chaine de recherche : "
						read c1
						cexec="$cexec -o"
						cexec=$(POS_NEG "$cexec" $c1)
						;;
					"STOP")
						break
						;;
					*) break;;
				esac
			done
			#echo -e "\n\nRésultats :"
			#eval $cexec

			eval $cexec > res_find
			echo "Nombre de lignes récupérées par la recherche : "
			echo res_find | wc -l 
            ;;

        "Sortir")
			break;;
        *) echo invalid option;;
    esac
    PS3='Veuillez choisir une méthode de recherche : '
    
	if [[ -z "$1" ]]
	then
		cexec="find \"$(pwd)\""
	else
		cexec="find \"$1\""
	fi

done
