#!/bin/bash

if [[ -z "$1" ]]
then
	pwd
else
	cd $1
fi

sed -i -e 's/ /\\ /g' res_find

PS3='Souhaitez vous créer un sous répértoire ?'
options=("Oui" "Non")
select opt in "${options[@]}"
do
    case $opt in
    	"Oui")
			echo "Veuillez saisir un nom de répertoire"
			read name
			mkdir $name
			mv res_find $name
			cd $name
			break
			;;
		"Non")

			break
			;;
		*)
			break;;
	esac
done

PS3='Souhaitez vous copier les résultats de la recherche précédente dans le dossier (courant) ?'
options=("Oui" "Non")
select opt in "${options[@]}"
do
    case $opt in
    	"Oui")

			while read p; do
  				cp "$p" ./
			done <res_find
			rm res_find

			break
			;;
		"Non")

			break
			;;
		*)
			break;;
	esac
done

PS3='Souhaitez vous modifier le noms des fichiers dans le dossier courant ?'
options=("Oui" "Non")
select opt in "${options[@]}"
do
    case $opt in
    	"Oui")

			echo "Veuillez saisir la chaine à remplacer :"
			read c1
			echo "Veuillez saisir la chaine de remplacement :"
			read c2
			
			for f in *"$c1"*; do mv "$f" "`echo $f | sed s/$c1/$c2/g`"; done

			break
			;;
		"Non")

			break
			;;
		*)
			break;;
	esac
done

