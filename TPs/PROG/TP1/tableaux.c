#include <stdio.h>
#include <time.h>

void afficherTab(int t[], int n) {
	
	int i;

	printf("{");
	for(i=0; i < n; i++) {
		printf(" %d ", t[i]);
	}
	printf("}\n");
}

void plusGrand(int t[], int n) {

	int i;
	int pG = t[0];

	for(i=1; i<n ;i++) {
		if(t[i] > pG) {
			pG = t[i];
		}
	}

	printf("Plus grand = %d \n", pG);
}

void deuxiemePlusGrand(int t[], int n) 
{

	int i;
	int res[2] = {t[0],t[1]};

	for(i = 1; i < n; i++) {
		if(t[i] > res[0]) {
			res[1] = res[0];
			res[0] = t[i];
		} else if( t[i] < res[0] && t[i] > res[1]) {
			res[1] = t[i];
		}
	}

	printf("Deuxième plus grand = %d \n", res[1]);
}

void main() {

	srand(5);

	int tab[10] = {rand()%100,rand()%100,rand()%100,rand()%100,rand()%100,
			rand()%100,rand()%100,rand()%100,rand()%100,rand()%100};

	afficherTab(tab, 10);
	
	plusGrand(tab,10);

	deuxiemePlusGrand(tab, 10);
	
}
