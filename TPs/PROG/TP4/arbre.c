#include <stdio.h>
#include <stdlib.h>
#include "arbre.h"

arbre nouvArbre(){

	/*arbre a = (arbre *)malloc(sizeof(arbre));
	a->val = NULL;
	a->fg = NULL;
	a->fd = NULL;
	return a;*/
	return NULL;
}

arbre enrac(T racine, arbre fg, arbre fd){

	arbre a = (arbre *)malloc(sizeof(arbre));
	a->val = racine;
	a->fg = fg;
	a->fd = fd;
	return a;
}

arbre creerFeuille(T val) {

	arbre a = (arbre *)malloc(sizeof(arbre));
	a->val = val;
}

T racine(arbre a){
	return a->val;
}

arbre fg(arbre a){
	return a->fg;
}

arbre fd(arbre a){
	return a->fd;
}

int taille(arbre a){
	int taille_fg;
	int taille_fd;

	if(a == NULL) {
		return 0;
	} else if(a->fg == NULL && a->fd == NULL){
		return 1;
	} else {
		taille_fg = taille(a->fg);
		taille_fd = taille(a->fd);

		return taille_fg+taille_fd;
	}
}

int estVide(arbre a){
	/*if(a->val == NULL && a->fg == NULL && a->fd == NULL) {
		return 1;
	} else {
		return 0;
	}*/
	if(a==NULL){
		return 1;
	} else {
		return 0;
	}
}

int estFeuille(arbre a){
	if(a->val != NULL && a->fg == NULL && a->fd == NULL) {
		return 1;
	} else {
		return 0;
	}
}

int hauteur(arbre a){

	int hauteur_fg;
	int hauteur_fd;
	if(a==NULL) {
		return 0;
	} else if (a->fg == NULL && a->fd == NULL) {
		return 1;
	} else{
		hauteur_fg = hauteur(a->fg);
		hauteur_fd = hauteur(a->fd);

		return hauteur_fg > hauteur_fd ? hauteur_fg : hauteur_fd;
	}
}

void parcoursPrefixe(arbre a, void (*affiche)(T)){


	if(!estVide(a)){
		affiche(a->val);
		parcoursPrefixe(a->fg,affiche);
		parcoursPrefixe(a->fd,affiche);
	}
	return;
}

void parcoursInfixe(arbre a, void (*affiche)(T)){
	if(!estVide(a)){
		parcoursInfixe(a->fg,affiche);
		affiche(a->val);
		parcoursInfixe(a->fd,affiche);
	}
	return;
}

void parcoursPostfixe(arbre a, void (*affiche)(T)){

	if(!estVide(a)){
		parcoursPostfixe(a->fg,affiche);
		parcoursPostfixe(a->fd,affiche);
		affiche(a->val);
	}
	return;
}

