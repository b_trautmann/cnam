#include <gmp.h>
#include <stdio.h>

void puissXIte (mpz_t x, int n, mpz_t * r) {

	int i;
	mpz_set_si(*r,1);
	
	for(i=0;i<n;i++){
		mpz_mul(*r,x,*r);
	}
	return;
}

void puissXRec1 (mpz_t x, int n, mpz_t * r) {

	mpz_t r2;
	mpz_init(r2);

	if(n==0){mpz_set_si(*r,1);}
	else {
		puissXRec1(x,n-1,&r2);
		mpz_mul(x,r2,*r);
	}
	return;
}

void puissXRec2 (mpz_t x, int n, mpz_t * r){
	
	mpz_t r2;
	mpz_init(r2);

	if(n==0){
		mpz_set_si(*r,1);
	}
	else{ 
		if(n%2==0){
			puissXRec2(x,n/2,&r2);
			mpz_mul(r2,r2,*r);
		} else {
			puissXRec2(x,n-1,&r2);
			mpz_mul(x,r2,*r);
		}
	}
}

void puissXRec3 (mpz_t x, int n, mpz_t * r) {

	mpz_t r2;
	mpz_init(r2);

	if(n==0){mpz_set_si(*r,1);}
	else{
		if(n%2==0){
			mpz_mul(x,x,r2);
			puissXRec3(r2,n/2,r);
		} else {
			puissXRec3(x,n-1,&r2);
			mpz_mul(x,r2,*r);
		}
	}
}

void main() {

	mpz_t x;
	mpz_init(x);
	mpz_set_si(x,2);

	mpz_t z;
	mpz_init(z);
	mpz_set_si(z,10);

	mpz_t r;
	mpz_init(r);

	mpz_t r2;
	mpz_init(r2);

	int n = 12;

	gmp_printf("x = %Zd, n = %d\n",x,n);

	puissXIte(x,n,&r);
	gmp_printf("Res1 = %Zd\n",r);

	puissXRec1(x,n,&r);
	gmp_printf("Res2 = %Zd\n",r);

	puissXRec2(x,n,&r);
	gmp_printf("Res3 = %Zd\n",r);

	puissXRec3(x,n,&r);
	gmp_printf("Res4 = %Zd\n",r);

	n = 1;
	puissXRec3(z,n,&r);
	puissXRec3(z,n+1,&r2);

	while(mpz_cmp(r,r2) > 0){
		n++;
		puissXRec3(z,n,&r);
		puissXRec3(z,n+1,&r2);
	}
	printf("n = %d",n);

	/*while(mpz_cmp(puissXIte(z,n),puissXIte(z,n+1)) == 0){
		n++;	
	}
	gmp_printf("n = %zd\n",n);
	*/
	return;
}

