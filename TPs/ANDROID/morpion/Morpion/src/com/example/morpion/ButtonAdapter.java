package com.example.morpion;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class ButtonAdapter extends BaseAdapter {

	private Context mContext;
	private ArrayList<Case> cases;

	public ButtonAdapter(Context c, ArrayList<Case> cases) {
		mContext = c;
		this.cases = cases;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return cases.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ImageView button;
		
		if(convertView == null) {
			button = new ImageView(mContext);
			button.setLayoutParams(new GridView.LayoutParams(142,142)); //Trouver des valeurs relatives ici
			button.setBackgroundColor(Color.BLACK);
		} else {
			button = (ImageView) convertView;
		}
		
		button.setId(position);
		return button;
	}
	
	
}
