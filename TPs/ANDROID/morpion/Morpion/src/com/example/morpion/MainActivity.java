package com.example.morpion;

import java.lang.reflect.Array;
import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;


public class MainActivity extends Activity {

	ArrayList<Case> cases;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GridView g = (GridView) findViewById(R.id.gridview);
        
        cases = new ArrayList<Case>();
        
        for(int i=0; i<9;i++){

        	Case b = new Case(this);
        	cases.add(b);
        }
        g.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Drawable croix = new Rond();
				view.setBackground(croix);
				cases.get(position).setEtat(1);
				for(Case b: cases){
					System.out.println(b.getEtat());
				}
			}
        	
        });
        
        g.setAdapter(new ButtonAdapter(this, cases));
    }

    public void VerifierEtat(ArrayList<Case> cases){
    	
    }
    
    public boolean victoireVert(ArrayList<Case> cases){
    	boolean res = false;
    	
    	if( (cases.get(0).getEtat() != 0 && cases.get(0).getEtat() == cases.get(3).getEtat() && cases.get(0).getEtat() == cases.get(6).getEtat()) ||
    			(cases.get(1).getEtat() != 0 && cases.get(1).getEtat() == cases.get(4).getEtat() && cases.get(1).getEtat() == cases.get(7).getEtat()) ||
    			(cases.get(2).getEtat() != 0 && cases.get(2).getEtat() == cases.get(5).getEtat() && cases.get(2).getEtat() == cases.get(8).getEtat())){
    		res = true;
    	}
    	
    	return res;
    }

    public boolean victoireHori(ArrayList<Case> cases){
    	boolean res = false;
    	
    	if( (cases.get(0).getEtat() != 0 && cases.get(0).getEtat() == cases.get(1).getEtat() && cases.get(0).getEtat() == cases.get(2).getEtat()) ||
    			(cases.get(3).getEtat() != 0 && cases.get(3).getEtat() == cases.get(4).getEtat() && cases.get(3).getEtat() == cases.get(5).getEtat()) ||
    			(cases.get(6).getEtat() != 0 && cases.get(6).getEtat() == cases.get(7).getEtat() && cases.get(6).getEtat() == cases.get(8).getEtat())){
    		res = true;
    	}
    	
    	return res;
    }
    
    public boolean victoireDiag(ArrayList<Case> cases){
    	boolean res = false;
    	
    	if( (cases.get(0).getEtat() != 0 && cases.get(0).getEtat() == cases.get(1).getEtat() && cases.get(0).getEtat() == cases.get(2).getEtat()) ||
    			(cases.get(3).getEtat() != 0 && cases.get(3).getEtat() == cases.get(4).getEtat() && cases.get(3).getEtat() == cases.get(5).getEtat()) ||
    			(cases.get(6).getEtat() != 0 && cases.get(6).getEtat() == cases.get(7).getEtat() && cases.get(6).getEtat() == cases.get(8).getEtat())){
    		res = true;
    	}
    	
    	return res;
    }  
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
