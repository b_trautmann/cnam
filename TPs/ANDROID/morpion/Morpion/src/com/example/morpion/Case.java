package com.example.morpion;

import android.content.Context;
import android.widget.ImageView;

public class Case extends ImageView {

	int etat = 0;
	
	public Case(Context context) {
		super(context);
	}
	
	public int getEtat(){
		return etat;
	}
	
	public void setEtat(int etat){
		this.etat = etat;
		return;
	}

}
