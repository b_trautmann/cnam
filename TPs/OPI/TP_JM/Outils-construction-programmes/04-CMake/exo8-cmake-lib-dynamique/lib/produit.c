#include <stdio.h>
#include "defs.h"
struct polynome saisie(void);
struct polynome mult (struct polynome, struct polynome);

/* ************************************************
   produit 
   Produit de 2 polynomes saisis au sein de la fonction
   entree : -
   sortie : -
**************************************************** */
void produit(void) {
   struct polynome P1,P2,Q;
   int i;
   printf("Premier polynome : \n");
   P1=saisie();
   printf("Second polynome : \n");
   P2=saisie();
   Q=mult(P1,P2);
   for(i=Q.degre; i>=0; i--)
      printf("coefficient de X a la puissance %d : %d\n",i, Q.coef[i]);
   printf("\n");
}
