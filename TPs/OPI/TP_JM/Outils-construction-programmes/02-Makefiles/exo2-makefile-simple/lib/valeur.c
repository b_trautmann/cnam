#include <math.h>
#include "defs.h"


/* ************************************************
   valeur
   Calcul de P pour une valeur X donnee
   entree : polynome P, valeur X
   sortie : P(X)
**************************************************** */
float valeur (struct polynome P, float X) {
   float resultat=0;
   int i;
   for (i=0; i<=P.degre; i++) {
      resultat += P.coef[i]*pow((double)X, (double)i);
   }
   return resultat;
}
