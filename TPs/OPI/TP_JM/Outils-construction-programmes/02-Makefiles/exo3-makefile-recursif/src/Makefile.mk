VPATH = ../header 
CC = gcc
LIB = ../lib
LIBS = -lm
HEADERS = ../header/
OPT := -I$(HEADERS)
SRC := $(notdir $(wildcard *.c))
OBJ := $(patsubst %.c, %.o, $(SRC))

all: prog

prog: $(OBJ)
	$(CC) $^ -L$(LIB) -lpromat $(LIBS) -o prog; mv prog ../

%.o: %.c defs.h
	$(CC) -c $< $(OPT);

mrproper: clean

clean:
	rm -f *.o
