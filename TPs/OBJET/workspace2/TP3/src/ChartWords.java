import java.awt.*; 
import java.awt.event.*; 
import javax.swing.*; 
import org.jfree.chart.*; 
import org.jfree.chart.plot.*; 
import org.jfree.data.*; 
import org.jfree.data.general.DefaultPieDataset;

// Classe gérant la création du camembert en fonction du WordAnalyser passé
public class ChartWords extends JFrame { 
  private JPanel pnl; 

  public ChartWords(WordAnalyzer wa) { 
    addWindowListener(new WindowAdapter() { 
      public void windowClosing(WindowEvent e) { 
        dispose(); 
        System.exit(0); 
      } 
    }); 
    pnl = new JPanel(new BorderLayout()); 
    setContentPane(pnl); 
    setSize(400, 250); 

    DefaultPieDataset pieDataset = new DefaultPieDataset();
    
    
    for(int i=0;i<10 && i<  wa.getWords().size();i++){
    	Word w = wa.getWords().get(i);
    	pieDataset.setValue(w.getWord(), new Integer(w.getFrequence())); 
    } 

    JFreeChart pieChart = ChartFactory.createPieChart("Camembert de la fréquence des 10 mots les plus fréquents", 
      pieDataset, true, true, true); 
    ChartPanel cPanel = new ChartPanel(pieChart); 
    pnl.add(cPanel); 
  } 

}
