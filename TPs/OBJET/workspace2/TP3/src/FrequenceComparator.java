import java.util.Comparator;


public class FrequenceComparator implements Comparator<Word>{

	@Override
	public int compare(Word o1, Word o2) {
		if(o1.getFrequence()>o2.getFrequence()){
			return -1;
		} else if(o1.getFrequence()<o2.getFrequence()) {
			return 1;
		}
		return 0;
	}

}
