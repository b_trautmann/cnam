import java.awt.*; 
import java.awt.event.*; 
import javax.swing.*; 
import org.jfree.chart.*; 
import org.jfree.chart.plot.*; 
import org.jfree.data.*; 
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

//Classe gérant la création de l'histogramme en fonction du WordAnalyser passé
public class HistWords extends JFrame { 
  private JPanel pnl; 

  public HistWords(WordAnalyzer wa) { 
	addWindowListener(new WindowAdapter() { 
      public void windowClosing(WindowEvent e) { 
        dispose(); 
        System.exit(0); 
      } 
    }); 
    pnl = new JPanel(new BorderLayout()); 
    setContentPane(pnl); 
    setSize(400, 250); 

    DefaultCategoryDataset dataset = new DefaultCategoryDataset(); 
    
    int[] frequences = wa.getFrequences();
    
    for(int i=0; i<frequences.length;i++){
    	dataset.addValue(frequences[i], "Fréquence", ""+i); 
    }

    JFreeChart barChart = ChartFactory.createBarChart("Fréquence des mots contenus dans le fichier par taille", "", 
      "Unité vendue", dataset, PlotOrientation.VERTICAL, true, true, false); 
    ChartPanel cPanel = new ChartPanel(barChart); 
    pnl.add(cPanel); 
  } 

}
