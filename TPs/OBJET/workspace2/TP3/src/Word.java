
public class Word {

	private String word;
	private int frequence;

	public Word(String word, int frequence) {
		super();
		this.word = word;
		this.frequence = frequence;
	}
	
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public int getFrequence() {
		return frequence;
	}
	public void setFrequence(int frequence) {
		this.frequence = frequence;
	}
	
	public String toString(){
		return this.word+" "+this.frequence;
	}

}
