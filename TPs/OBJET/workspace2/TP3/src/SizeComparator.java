import java.util.Comparator;


public class SizeComparator implements Comparator<Word>{

	@Override
	public int compare(Word o1, Word o2) {
		if(o1.getWord().length()>o2.getWord().length()){
			return -1;
		} else if(o1.getWord().length()<o2.getWord().length()) {
			return 1;
		}
		return 0;
	}

}
