import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public class WordAnalyzer {
	
	private ArrayList<Word> words;
	private int[] frequences;

	public WordAnalyzer() {
		super();
		this.words = new ArrayList<Word>();
	}
	
	public void addWord(String word){
		if(this.containsKey(word)){
			this.addFrequence(word);
		} else {
			this.words.add(new Word(word,1));
		}
		Collections.sort(this.words, new FrequenceComparator());
		//System.out.println(this.words.toString());
	}
	
	public boolean containsKey(String s){
		for(Word w: words){
			if(w.getWord().compareTo(s)==0){
				return true;
			}
		}
		return false;
	}
	
	public void addFrequence(String s){
		for(Word w: words){
			if(w.getWord().compareTo(s)==0){
				w.setFrequence(w.getFrequence()+1);
				return;
			}
		}
		return;
	}
	
	public int getFrequence(String s){
		for(Word w: words){
			if(w.getWord().compareTo(s)==0){
				w.setFrequence(w.getFrequence()+1);
				return w.getFrequence();
			}
		}
		return 0;
	}
	
	public String getRandomFrequence(){
		Random generator = new Random();
		int random = generator.nextInt(words.size());
		Word word = words.get(random);
		
		return "Fréquence \""+word.getWord()+"\" : "+word.getFrequence();
	}
	
	public void processFrequencetab(){
		int max = 0;
		for(Word w: words){
			if(w.getFrequence()>max){
				max=w.getFrequence();
			}
		}
		frequences = new int[max];
		for(Word w: words){
			frequences[w.getFrequence()-1]++;
		}
		return;
	}
	
	public void printFrequenceTab(){
		int i, i2;
		
		System.out.println("Répartition des fréquences");
		for(i=0;i<frequences.length;i++){
			System.out.print(i+" ");
			for(i2=0;i2<frequences[i];i2++){
				System.out.print("*");
			}
			System.out.print("\n");
		}
	}

	public int[] getFrequences() {
		return frequences;
	}

	public void setFrequences(int[] frequences) {
		this.frequences = frequences;
	}

	public ArrayList<Word> getWords() {
		return words;
	}

	public void setWords(ArrayList<Word> words) {
		this.words = words;
	}
}
