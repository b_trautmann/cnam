import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics; 
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.image.BufferedImage; 
import java.io.File; 
import java.io.IOException; 
import java.net.URL; 
import javax.imageio.ImageIO; 
import javax.swing.JPanel; 
 

public class JImagePanel extends JPanel {

    private BufferedImage img;
    private BufferedImage renderImg;
    private boolean rounded;

    public JImagePanel() {
    }

    public void setRounded(boolean value) {
        if (value != rounded) {
            rounded = value;
            renderImg = null;
            firePropertyChange("rounded", !rounded, rounded);
            repaint();
        }
    }

    public boolean isRounded() {
        return rounded;
    }

    public void setImage(ImageFile valueIF) {
    	BufferedImage value = valueIF.getBimg();
        BufferedImage old = img;
        img = value;
        renderImg = null;
        firePropertyChange("image", old, img);
        repaint();
    }

    public BufferedImage getImage() {
        return img;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension size = img == null ? new Dimension(200, 200) : new Dimension(img.getWidth(), img.getHeight());
        Insets insets = getInsets();
        size.width += (insets.left + insets.right);
        size.height += (insets.top + insets.bottom);
        return size;
    }


    protected BufferedImage getImageToRender() {

        if (renderImg == null) {
            BufferedImage source = getImage();
            if (source != null) {
                if (isRounded()) {
                    BufferedImage mask = new BufferedImage(source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_ARGB);
                    Graphics2D g2d = mask.createGraphics();
                    g2d.setBackground(new Color(255, 255, 255, 0));
                    g2d.clearRect(0, 0, mask.getWidth(), mask.getHeight());
                    g2d.setBackground(new Color(255, 255, 255, 255));
                    g2d.fillRoundRect(0, 0, mask.getWidth(), mask.getHeight(), 40, 40);
                    g2d.dispose();

                    BufferedImage comp = new BufferedImage(source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_ARGB);
                    g2d = comp.createGraphics();
                    g2d.setBackground(new Color(255, 255, 255, 0));
                    g2d.clearRect(0, 0, source.getWidth(), source.getHeight());
                    g2d.drawImage(source, 0, 0, this);
                    g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.DST_IN));
                    g2d.drawImage(mask, 0, 0, this);
                    g2d.dispose();

                    renderImg = comp;
                } else {
                    renderImg = source;
                }
            }
        }

        return renderImg;

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        BufferedImage img = getImageToRender();
        if (img != null) {
            Insets insets = getInsets();
            Graphics2D g2d = (Graphics2D) g.create();
            int width = getWidth();
            int height = getHeight();
            int x = ((width - img.getWidth()) / 2);
            int y = ((height - img.getHeight()) / 2);
            g2d.drawImage(img, x, y, this);
            g2d.dispose();
        }
    }
}