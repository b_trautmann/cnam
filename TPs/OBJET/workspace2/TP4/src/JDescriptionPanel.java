import java.awt.GridLayout;
import java.awt.Label;

import javax.swing.JPanel;


public class JDescriptionPanel extends JPanel {
	  public JDescriptionPanel() {
		  super();
	  }
		  
	  public JDescriptionPanel(ImageFile img) {
	  	super();
	    this.setLayout(new GridLayout(0,2));
	    
	    this.add(new Label("Largeur : "+img.getBimg().getWidth()));
	    this.add(new Label("Hauteur : "+img.getBimg().getHeight()));

	    this.add(new Label("Date de création : "+img.getAttr().creationTime()));
	    this.add(new Label("Dernière modification : "+img.getAttr().lastModifiedTime()));

	    this.add(new Label("Taille : "+img.getAttr().size()));
	  }
}
