
public class PointCol extends Point {

	public int couleur;
	
	public PointCol(int x, int y, int c) {
		super(x, y);
		couleur = c;
	}

	public int getCouleur() {
		return couleur;
	}

	public void setCouleur(int couleur) {
		this.couleur = couleur;
	}
	
	public String printPoint(){
		return super.printPoint()+" Couleur : ["+this.getCouleur()+"]";
	}
	
}
