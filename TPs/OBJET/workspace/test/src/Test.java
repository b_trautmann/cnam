import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.MenuBar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.*;
import javax.swing.*;

import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;


public class Test {
	public static void main_old(String[] args){
		DataInputStream d = new DataInputStream(System.in);
		System.out.println("==>Taper deux lignes");
		try{
			System.out.print("?");
			String ligne1 = d.readLine();
			System.out.print("?");
			String ligne2 = d.readLine();
			System.out.println("Les deux lignes lues sont :"+ ligne1 +"=et="+ligne2);
			
		}catch(java.io.IOException e){
			System.out.println("Il y a une erreur de lecture");
			
		}
	}
	
	public static void main_old2(String[] args) throws IOException{
		BufferedReader lecteurAvecBuffer = null;
		String ligne;
		
		try{
			lecteurAvecBuffer = new BufferedReader(new FileReader(args[0]));
		} catch(FileNotFoundException exc){
			System.out.println("Erreur d'ouverture");
		}
		while (( ligne = lecteurAvecBuffer.readLine()) != null){
			
			System.out.println(ligne);
		}
		lecteurAvecBuffer.close();
		
		
	}
	
	/*public static void main(String args[]){
		
		String fileName = "data.txt";
		try{
			List<String> lines = Files.readAllLines(Paths.get(fileName), Charset.defaultCharset());
			for(String line : lines){
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}*/
	
	public static void main_old4(String args[]){
		StringTokenizer istMots = new StringTokenizer(args[0]," ,.");
		int nombreDeMots = istMots.countTokens();
		System.out.println("==> Dans la phrase :"+args[0]+", il y a "+nombreDeMots+" mots qui sont");
		int i =0;
		while(i++<nombreDeMots){
			
			System.out.println("==> "+i+"->"+istMots.nextToken());
		}
	}
	
	public static void main_old5(String args[]){
		StringTokenizer istMots = new StringTokenizer(args[0]," l,.");
		Vector vecteur = new Vector();
		while(istMots.hasMoreTokens()){
			String mot = istMots.nextToken();
			boolean dejaVu = false;
			
			for(int i=0; i<vecteur.size();i++){
				String motMemorise = (String) vecteur.elementAt(i);
				dejaVu = motMemorise.equals(mot);
				if(dejaVu){break;}
				
			}
			if(!dejaVu){ vecteur.addElement(mot);}
			
		}
		System.out.println("==> Dans la phrase "+args[0]+" === il y a "+vecteur.size()+ " mots différents qui sont:");
		
		for(int i=0; i<vecteur.size();i++){
			
			System.out.println("==>"+i+"->"+vecteur.elementAt(i));
		}
	}
	
	private static void createAndShowGui(){
		
			JFrame frame = new JFrame("HelloWolrdSwing");
			
			BorderLayout layout = new BorderLayout();
			
			JMenuBar menubar = new JMenuBar();
			
			JMenu menu = new JMenu("Un menu");
			
			menubar.add(menu);
			
			JMenuItem menuItem = new JMenuItem("A text-only menu item",
                    KeyEvent.VK_T);
			
			menu.add(menuItem);
			
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			frame.setLayout(layout);
			
			JLabel label = new JLabel("Hello World");
			
			frame.getContentPane().add(label, BorderLayout.NORTH);
			
			JButton b = new JButton("Mon bouton");
			
			b.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
	            System.out.println("Quelqu'un à cliqué sur le bouton hello world");
			        }
			});
			
			frame.getContentPane().add(b, BorderLayout.CENTER);
			
			frame.setJMenuBar(menubar);
			
			frame.pack();
			
			frame.setVisible(true);
	}
	
	public static void main(String args[]){
		
		javax.swing.SwingUtilities.invokeLater(new Runnable(){
			
			public void run() {
				createAndShowGui();
			}
		});
	}
	

}



