import java.util.ArrayList;


public class Catalogue {
	private final ArrayList<Prestation> prestations;
	
	Catalogue(){
		super();
		this.prestations = new ArrayList<Prestation>();
		this.init();
	}
	
	private void init(){
		Prestation pres1 = new Prestation("Réparation 1",156.12,Prestation.Categorie.CARROSSERIE);
		Prestation pres2 = new Prestation("Réparation 2",136.89,Prestation.Categorie.PEINTURE);
		Prestation pres3 = new Prestation("Réparation 3",90.50,Prestation.Categorie.MECANIQUE);
		
		prestations.add(pres1);
		prestations.add(pres2);
		prestations.add(pres3);
	}

	public ArrayList<Prestation> getPrestations() {
		return prestations;
	}
	
	public String toString(){
		String res ="";
		for(int i=0;i<this.prestations.size();i++){
			res += this.prestations.get(i)+"\n\n";
		}
		return res;
	}
	
	public void ajouterPrestation(Prestation pres){
		this.prestations.add(pres);
	}
	
	public boolean isInCatalogue(Prestation prestation){
		boolean res=false;
		for(int i=0;i<this.prestations.size();i++){
			if(prestation == this.prestations.get(i)){
				res = true;
			}
		}
		return res;
	}
	
}
