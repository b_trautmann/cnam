import java.util.ArrayList;
import java.util.Date;


public class Simulation {

	Simulation() throws CatalogueException{
		init();
	}
	
	public static void main(String [] args) throws CatalogueException{
		new Simulation();
	}
	
	private void init() throws CatalogueException{
		Client c1 = new Client("jean-charles","peloutier","0666666666");
		Client c2 = new Client("mohamed","reqba","0777777777");
		Client c3 = new Client("eric","rousseau","0888888888");
		
		System.out.println("############# PROPRIETAIRES\n\n");
		
		System.out.println(c1);
		System.out.println(c2);
		System.out.println(c3+"\n");
		
		Voiture v1 = new Voiture("Renault","Clio","1999",c1);
		Voiture v2 = new Voiture("Renault","Espace","2005",c2);
		Voiture v3 = new Voiture("Renault","Espace","2005",c3);
		
		Catalogue catalogue = new Catalogue();
		
		System.out.println("############# CATALOGUE\n\n");
		
		System.out.println(catalogue);
		

		v1.ajouterReparation(new Reparation(catalogue.getPrestations().get(0),new Date()),catalogue);
		v1.ajouterReparation(new Reparation(catalogue.getPrestations().get(1),new Date()),catalogue);
		
		
		v2.ajouterReparation(new Reparation(catalogue.getPrestations().get(2),new Date()),catalogue);
		
		System.out.println("############# VOITURE 1\n\n");
		System.out.println(v1);
		v1.afficherReparations();
		
		System.out.println("\n\n############# VOITURE 2\n\n");
		System.out.println(v2);
		v2.afficherReparations();

		System.out.println("\n\n############# LANCEMENT DE L'ERREUR\n\n");
		Prestation prestationError = new Prestation("Réparation 4",90.50,Prestation.Categorie.MECANIQUE);
		v3.ajouterReparation(new Reparation(prestationError,new Date()),catalogue);
	}
}
