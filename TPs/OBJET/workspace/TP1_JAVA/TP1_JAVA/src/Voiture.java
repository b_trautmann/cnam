import java.util.ArrayList;


public class Voiture {
	private final String marque;
	private final String modele;
	private final String anneeFabriquation;
	private final Client proprietaire;
	private ArrayList<Reparation> reparations;

	Voiture(String marque, String modele, String anneeFabriquation, Client proprietaire){
		super();
		this.marque = marque;
		this.modele = modele;
		this.anneeFabriquation = anneeFabriquation;
		this.proprietaire = proprietaire;
		this.reparations = new ArrayList<Reparation>();
	}

	public String getMarque() {
		return marque;
	}

	public Client getProprietaire() {
		return proprietaire;
	}

	public String getModele() {
		return modele;
	}

	public String getAnneeFabriquation() {
		return anneeFabriquation;
	}
	
	@Override
	public String toString(){
		return "Marque: "+this.marque+" \nModèle: "+this.modele+" \nAnnée de fabriquation: "+this.anneeFabriquation+" \nPropriètaire: "+this.proprietaire+"\n";
	}
	
	public void ajouterReparation(Reparation repa,Catalogue cata) throws CatalogueException{
		if(cata.isInCatalogue(repa.getPrestation())){
			this.reparations.add(repa);
		} else {
			throw new CatalogueException();
		}
	}
	
	public void afficherReparations(){
		for(int i=0;i<this.reparations.size();i++){
			System.out.println(this.reparations.get(i)+"\n");
			
		}
		System.out.println("\nMontant total: "+this.calculMontant());
	}
	
	public double calculMontant(){
		double res = 0;
		for(int i=0;i<this.reparations.size();i++){
			res += this.reparations.get(i).getMontant();
		}
		return res;
	}
	
}
