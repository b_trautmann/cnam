
public class Client {

	private final String nom;
	private final String prenom;
	private String telephone;

	Client( String prenom,String nom, String telephone){
		super();
		this.nom = formatNom(nom);
		this.prenom = formatNom(prenom);
		this.telephone = formatTel(telephone);
	}
	
	private static String formatNom(String val){
		String cap = val.substring(0, 1).toUpperCase();
	    String res = cap + val.substring(1).toLowerCase();
		return res;
	}
	
	private static String formatTel(String val){
		String res = val;
		if(val.charAt(0) == '0'){
			res = val.substring(1);
			res = "+33"+res;
		}
		
		res = String.format("%s %s %s %s %s %s", res.substring(0, 3), res.substring(3, 4), 
		          res.substring(4, 6), res.substring(6, 8), res.substring(8, 10), res.substring(10, 12));
		
		return res;
	}

	public String getNomPrenom() {
		return prenom+" "+nom;
	}
	
	@Override
	public String toString(){
		
		return this.getNomPrenom()+" "+this.telephone;
	}

	@Override
	public boolean equals(Object client){
		
		if (!(client instanceof Client))return false;
	    Client c = (Client)client;

		if(this.nom.equals(c.nom) && this.prenom.equals(c.prenom) && this.telephone.equals(c.telephone)){
			return true;
		} else {
			return false;
		}
		
	}
	
}
