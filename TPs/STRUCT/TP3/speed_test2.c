#include <stdio.h>
#include <stdlib.h>  // pour srand et rand
#include <time.h>    // pour srand
#include "liste_tableau.h"
#define TAILLE 100000
#define R 100

void affiche(liste l) {
	int i;
	
	for(i=0; i < taille(l); ++i) {
		printf("%d ", elt(i, l));
	}
	printf("\n");
}

void remplir_liste_tete(liste l) {

	int i = 0;

	while(i<TAILLE){
		adjt(rand()%100, l);
		i++;
	}
}

void remplir_liste_queue(liste l) {
	
	int i = 0;

	while(i<TAILLE){
		adjq(rand()%100, l);
		i++;
	}
}

long calcul_somme(liste l){
	
	int tailler = taille(l);
	int i = 0;
	long res=0;

	while(i<tailler){
		res = res + elt(i,l);
		i++;
	}
	return res;
}

void remplir_n(int n,liste l){

	int i = 0;

	while(i<n){
		adjt(rand()%100, l);
		i++;
	}
	
}

void main(){

	liste l = nouvListe();
	clock_t t1, t2;

	FILE *fp;  // pointeur sur le fichier
	    
	fp = fopen("fdata2", "w"); // ouverture (création et troncature si besoin)
	if (!fp) {
		perror("fopen :");
		exit(1);
	}


	
	double res;
	long secondes;

	int n = 5000;

	while(n<=70000){

		t1 = clock();
		remplir_n(n,l);
		t2 = clock();

		free(l);
		l = nouvListe();

		//fprintf(fp,"%d  :  ",n);
		//printf("%f\n",res);
		
		fprintf(fp,"%d %f\n", n, (t2-t1)/(float)CLOCKS_PER_SEC);
		fflush(fp);
		n=n+5000;
	}
	
	fclose(fp);
    	exit(0);

}
