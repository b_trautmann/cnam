#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include "pile.h"

pile pileNouv(){
	pile p = (pile)malloc(sizeof(struct PILE));
	p->premier = NULL;
	p->taille = 0;
	return p;
}

pile empiler(pile p, T val){
	char* res = (char *)malloc(sizeof(val));
	res = strcpy(res,val);
	elt e = (elt)malloc(sizeof(struct ELT));
	e->s = p->premier;	
	e->e = res;	
	p->premier = e;
	p->taille++;
	return p;
}

pile depiler(pile p){
	elt del = p->premier;
	p->premier = p->premier->s;
	free(del);
	p->taille--;
	return p;
}

int taille(pile p){
	return p->taille;
}

int estVide(pile p){
	if(p->premier == NULL){
		return 1;
	} else { return 0;}
}

T sommet(pile p){
	if(p->premier != NULL){
		return p->premier->e;
	} else { return NULL;}
}
