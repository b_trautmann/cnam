#include <stdio.h>
#include <stdlib.h>  // pour srand et rand
#include <time.h>    // pour srand
#include "liste_tableau.h"
#define TAILLE 100000
#define R 100

void affiche(liste l) {
	int i;
	
	for(i=0; i < taille(l); ++i) {
		printf("%d ", elt(i, l));
	}
	printf("\n");
}

void remplir_liste_tete(liste l) {

	int i = 0;

	while(i<TAILLE){
		adjt(rand()%100, l);
		i++;
	}
}

void remplir_liste_queue(liste l) {
	
	int i = 0;

	while(i<TAILLE){
		adjq(rand()%100, l);
		i++;
	}
}

long calcul_somme(liste l){
	
	int tailler = taille(l);
	int i = 0;
	long res=0;

	while(i<tailler){
		res = res + elt(i,l);
		i++;
	}
	return res;
}

void main(){

	clock_t start_t, end_t, end2_t, total_remp1, total_remp2;

	srand(R);
	liste l1 = nouvListe();
	liste l2 = nouvListe();
	
	start_t = clock();
   	printf("Début remplissage tete = %ld\n", start_t);

	remplir_liste_tete(l1);

	end_t = clock();
   	printf("Fin remplissage tete = %ld\n", end_t);

	remplir_liste_queue(l2);

	end2_t = clock();
	printf("Fin remplissage queue = %ld\n",end2_t);

	total_remp1 = (end_t - start_t);
	total_remp2 = (end2_t - end_t);

	printf("Temps remplissage tete = %ld\n",total_remp1);
	printf("Temps remplissage queue = %ld\n",total_remp2);

	printf("%d\n",calcul_somme(l2));
	
}
