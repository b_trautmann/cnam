#include <stdio.h>
#include <stdlib.h>
#include "liste.h"

void afficherListe(liste l) {

	int i;
	int n = taille(l);

	printf("[");
	for(i=0;i<n;i++){
		printf(" %d ;",elt(i,l));
	}
	printf(" ]\n");
}

int nbOccurences(liste l, int val) {

	int i, res=0;

	for(i=0;i<taille(l);i++){
		if(elt(i,l)==val){
			res++;
		}
	}

	return res;
}

int cherche(liste l, int val) {

	int i;

	for(i=0;i<taille(l);i++){
		if(elt(i,l)==val){
			return i;
		}
	}

	return -1;	
}

liste concat(liste l1, liste l2){

	int i;
	liste res = nouvListe();

	for(i=0;i<taille(l1);i++){
		adjq(elt(i,l1),res);
	}

	for(i=0;i<taille(l2);i++){
		adjq(elt(i,l2),res);
	}

	return res;
}

liste trie(liste l) {

	int i,j, val;
	liste res = nouvListe();

	for(i=0;i<taille(l);i++){
		val = elt(i,l);
		for(j=0;j<taille(res);j++){
			if( elt(j,res) > val ) {	
				break;
			}
		}
		insert(val,res,j);
	}

	return res;
}

liste interclass(liste l1, liste l2){

	int i=0; int j=0;
	liste res = nouvListe();

	while(i< taille(l1) && j < taille(l2)){
		if(elt(i,l1)>elt(j,l2)){
			adjq(elt(j,l2),res);
			j++;
		} else {
			adjq(elt(i,l1),res);
			i++;
		}
	}

	if(i==taille(l1)){
		for(j;j<taille(l2);j++){
			adjq(elt(j,l2),res);
		}
	} else if(j==taille(l2)){
		for(i;i<taille(l1);i++){
			adjq(elt(i,l1),res);
		}
	}

	return res;

}

int palindrome(liste l1) {

	int i;

	for(i=0;i<taille(l1)/2;i++){
		if(elt(i,l1) != elt(taille(l1)-i,l1)){
			return 1;
		}
	}
	return 0;
}

void main() {

	liste li1 = nouvListe();
	liste li2 = nouvListe();
	liste li3 = nouvListe();
	liste li4 = nouvListe();
	liste li5;
	liste li6;
	int var,i;

	srand(12);

	for(i=0;i<15;i++){
		adjq(rand()%100,li4);
	}

	for(i=0;i<15;i++){
		adjq(rand()%100,li1);
	}

	for(i=0;i<15;i++){
		adjq(rand()%100,li2);
	}

	//afficherListe(li4);

	//afficherListe(li5 = trie(li4));


	li3 = trie(li1);
	afficherListe(li3);
	li4 = trie(li2);
	afficherListe(li4);
	afficherListe(li6 = interclass(li3,li4));

	printf("%d",palindrome(li4));

	/*adjq(1,li1);
	adjq(8,li1);
	adjq(-4,li1);

	afficherListe(li1);

	scanf("%d",&var);

	while(var > 0){

		adjt(var,li2);
		scanf("%d",&var);
	}

	afficherListe(li2);

	scanf("%d",&var);

	i = 1;
	while(i <= var) {
		adjq(i*i, li3);
		i++;
	}

	afficherListe(li3);*/
}
