#include <stdio.h>
#include <stdlib.h>
#include "liste_tableau.h"
#define TAILLE    128

liste nouvListe(){
   
    liste l = (liste)malloc(sizeof(struct LISTE));
        
    l->tab = (int *)malloc(sizeof(int)*128);
    l->taille = 0;
    l->nb_elements = 0;

    return l;
}



static void ajuste_taille(liste l, int nb_elements) {
	int nb_blocs = (nb_elements/TAILLE)+1;
	int taille = TAILLE*nb_blocs;
	if (taille != l->taille) {
		l->taille = taille;
		l->tab = realloc(l->tab, sizeof(int)*taille);
	}
}

void adjt(int x, liste l) {
	int i;
	l->nb_elements++;
	ajuste_taille(l, l->nb_elements);
	// on décale tous les éléments
	for(i=l->nb_elements-2; i>=0; --i) {
		l->tab[i+1] = l->tab[i];
	}
	l->tab[0] = x;
	return;
}

void adjq(int x, liste l){

	int i;
	l->nb_elements++;
	ajuste_taille(l, l->nb_elements);

	l->tab[l->nb_elements-1] = x;
	return;
}

int taille(liste l){

	return l->nb_elements;
}

void insert(int e, liste l, int ind){

	int i;
	l->nb_elements++;
	ajuste_taille(l, l->nb_elements);

	for(i=l->nb_elements-2; i>=ind; --i) {
		l->tab[i+1] = l->tab[i];
	}
	l->tab[ind] = e;

	return;
}

int tete(liste l){
	return l->tab[0];
}

int queue(liste l){
	return l->tab[l->nb_elements-1];
}

void supt(liste l){
	int i;
	if (l->nb_elements <= 0) { return; }

	l->nb_elements--;
	for(i=0; i < l->nb_elements; ++i) {
		l->tab[i] = l->tab[i+1];
	}
	ajuste_taille(l, l->nb_elements);

	return;
}

void supq(liste l) {
	if (l->nb_elements <= 0) { return; }

	l->nb_elements--;
	ajuste_taille(l, l->nb_elements);

	return;
}

int estVide(liste l) {
	return (l->nb_elements > 0?0:1);
}

int elt(int ind, liste l) {
	if(ind<taille(l)){
		return l->tab[ind];
	} else {return 0;}
}

void fermeList(liste l){
	l->nb_elements=0;
	ajuste_taille(l, l->nb_elements);
	free(l);
}


