#include <stdio.h>
#include <stdlib.h>
#include "liste_chainee.h"
#define TAILLE    128

liste nouvListe() {
	liste l = (liste)malloc(sizeof(struct LISTE));
	l->premier = NULL;
	return l;
}

void adjt(int x, liste l) {
	elt_liste enouv = (elt_liste)malloc(sizeof(struct ELT));
	enouv->val = x;
	enouv->suiv = l->premier;
	l->premier = enouv;
	return;
}

void adjq(int x, liste l) {
	elt_liste enouv = (elt_liste)malloc(sizeof(struct ELT));
	elt_liste elt_courant = l->premier;
	enouv->val = x;
	enouv->suiv = NULL;
	if(elt_courant == NULL){
		l->premier = enouv;
		return;
	}
	while( elt_courant->suiv != NULL){ elt_courant = elt_courant->suiv;}
	elt_courant->suiv = enouv;
}

int taille(liste l) {
	int i = 0;
	elt_liste elt_courant = l->premier; 

	while(elt_courant != NULL){
		elt_courant = elt_courant->suiv;
		i++;
	}

	return i;
}

void insert(int e, liste l, int ind){

	int i = 0;
	elt_liste enouv = (elt_liste)malloc(sizeof(struct ELT));
	elt_liste elt_courant = l->premier;
	enouv->val = e;
	//enouv->suiv = null;

	while(elt_courant && elt_courant->suiv && i < ind){
		i++;
		elt_courant = elt_courant->suiv;
	}
	
	if(elt_courant) {
		if(  elt_courant->suiv) {
			enouv->suiv = elt_courant->suiv;
		}
		elt_courant->suiv = enouv;
	} else {l->premier = enouv;}

	
}

int tete(liste l){
	return l->premier->val;
}

int queue(liste l){

	elt_liste elt_courant = l->premier;
	while(elt_courant->suiv != NULL){
		elt_courant = elt_courant->suiv;
	}
	return elt_courant->val;
}

void supt(liste l){

	elt_liste premier = l->premier;
	l->premier = premier->suiv;
	free(premier);
}

void supq(liste l){

	elt_liste elt_courant = l->premier;
	while(elt_courant->suiv->suiv != NULL){
		elt_courant = elt_courant->suiv;
	}
	free(elt_courant->suiv);
	elt_courant->suiv = NULL;
	
}

int estVide(liste l){
	if(l->premier != NULL){
		return 0;
	} else {
		return 1;
	}
}

int elt(int ind, liste l){

	int i = 0;
	elt_liste elt_courant = l->premier;
	while(elt_courant->suiv != NULL && i < ind){
		i++;
		elt_courant = elt_courant->suiv;
	}
	return elt_courant->val;
}

void fermeListe(liste l){
	free(l);
}
