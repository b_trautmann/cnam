#include <stdio.h>
#include <stdlib.h>
#include "arbre.h"


void afficher(int val){

	printf(" %d ",val);
}

arbre * remplir_arbre(arbre * arbreval, int i){

	int randif;
	int randVal;
	int randVal2;
	while(i>0){
		randif = rand()%5;
		randVal = rand()%20;
		randVal2 = rand()%20;

		if(randif == 0){
			arbreval = arbreval;
		} else if(randif == 1){
			arbreval = enrac(randVal,arbreval,nouvArbre());
			remplir_arbre(fd(arbreval),i);
		} else if(randif == 2){
			arbreval = enrac(randVal,nouvArbre(),arbreval);
			remplir_arbre(fg(arbreval),i);
		} else if(randif == 3){
			arbreval = enrac(randVal,arbreval,enrac(randVal2,nouvArbre(),nouvArbre()));
		} else {
			arbreval = enrac(randVal,arbreval,enrac(randVal2,nouvArbre(),nouvArbre()));
		}
		i--;
	}

}

void main(){

	srand(time(NULL));
	int i = 20;

	arbre * arbre_test = nouvArbre();

	int randVal = rand()%20;
	//arbre * arbreval = enrac(rand()%20,arbre_test,nouvArbre());
	arbre * arbreval = enrac(rand()%20,nouvArbre(),nouvArbre());
	
	arbreval = remplir_arbre(arbreval,i);
	
	printf("\n------------ Parcours infixe ------------\n");
	parcoursInfixe(arbreval,*afficher);
	printf("\n------------ Parcours préfixe -----------\n");
	parcoursPrefixe(arbreval,*afficher);
	printf("\n------------ Parcours postfixe ----------\n");
	parcoursPostfixe(arbreval,*afficher);
	printf("\n");

	parcoursGenerationDot(arbreval);

}

