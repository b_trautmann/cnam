EXTERN	printf
EXTERN 	scanf

SECTION .data
	a	dd  5
	chainint db "%d",13,10,0
	chaine	db	"%d",0


SECTION .text

GLOBAL main

main:
	push a
	push chaine
	call scanf
	add esp,8
	
	mov 	eax,[a]
	imul 	eax,2
	add 	eax,1

	push 	eax
	push 	chainint
	call printf
	add	esp,8

	mov	eax,0
	ret
