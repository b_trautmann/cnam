EXTERN	printf
EXTERN 	scanf

SECTION .data
	a	dd  5
	chainint db "%d",13,10,0
	chaine	db	"%d",0


SECTION .text

GLOBAL main

main:
	mov	ebp,esp
	mov 	eax,[ebp+4]

	;Appel à argc placé sur esp+4

	push 	eax
	push 	chainint
	call printf
	add	esp,8

	mov	eax,0
	ret
