EXTERN	printf
EXTERN 	scanf

SECTION .data
	a	dd  5
	chainint db "%s",13,10,0
	chaine	db	"%d",0


SECTION .text

GLOBAL main

main:
	mov	ebp,esp
	mov 	esi,[ebp+8]
	mov	eax,[esi+4]

	;Appel à argv placé sur esp+8
	;Appel à argv[1] place sur esi+4

	push 	eax
	push 	chainint
	call printf
	add	esp,8

	mov	eax,0
	ret
