
EXTERN printf
EXTERN puts
EXTERN atoi
SECTION .DATA

chainint db "%d ",13,10,0

SECTION .TEXT
        GLOBAL  main

main:
        mov ebp, esp
        add ebp,4                    ; saute adresse de retour

        mov eax,[ebp]                ; recupere argc
        dec eax	                     ; eax = argc - 2	
        dec eax                      ; si eax diff de 2
        jnz end_err                  ; alors on sort

        add ebp,4                    ; passe a argv dans la pile
        mov esi,[ebp]                ; esi <- argv
        mov eax,[esi+4]              ; eax <- argv[1] 1=>+4 (car int = 4 octets)
	mov esi,[esi+4]

	mov ecx,32              	 ; ecx = 32

	push eax		; On passe EAX en paramètre à STRLEN
	call strlen		; Appel à STRLEN
	add esp,4		; Dépile
	
	add esi,esi,eax		; On passe ESI + EAX pour obtenir l'adresse dernier caractère de la chaine
	
boucle:                  	 ; TQ (ecx != 0)

	push esi                     ; empile le dernier caractère
        call atoi                    ; appel a atoi (resultat dans eax)
        add esp,4                    ; depile

	test eax,1
	jnz false		 ; Si le bit de poids faible == 0 jump à false
	mov al,'0'		; On enregistre 0 dans al
	jmp endif		; fin de if

false:
	mov al,'1'		; On enregistre 1 dans al
	jmp endif		; fin de if


endif:
	mov esi,al		; On écrit le contenu de al à l'adresse de esi
	dec esi			; On décale de 1 vers l'arrière ESI
	shr esi			; On décale les bits de la valeur à afficher

	loop boucle

	ret
